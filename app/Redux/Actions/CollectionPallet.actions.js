import {FourSeasonDictionary, LeafBackgroundColorArray, shuffle} from '../../Components/Common/GameConstants';

import {createActionSetActivityIndicatorFlag,} from '../Actions/Global.actions';
import {
    persistAppParamToStorage,
    initAppParamFromStorage,
} from '../../Utility/PersistDataHelper.js';

export const ACTION_SET_PALLET_SIZE = "SetPalletSize";
export const ACTION_SET_GRID_MATRIX = "InitGridMatrix";
export const ACTION_FIND_PATH = "FindPath";
export const ACTION_SET_LEVEL_DATA = "SetLevelData";
export const ACTION_SET_LEVEL_INDEX = "SetLevelIndex";

export function createActionSetPalletSize(rowCount, columnCount){
    return {type:ACTION_SET_PALLET_SIZE, size:{rowCount:rowCount, columnCount:columnCount}};
}

export function createActionSetGridMatrix(matrix){
    return {type:ACTION_SET_GRID_MATRIX, matrix:matrix};
}

export function createActionSetLevelData(levelData){
    return {type:ACTION_SET_LEVEL_DATA, levelData:levelData};
}

export function createActionSetLevelIndex(levelIndex){
    return {type:ACTION_SET_LEVEL_INDEX, levelIndex:levelIndex};
}

export function createActionInitializeCollectionPallet(){
    return async (dispatch, getState)=>{
        //dispatch(createActionSetActivityIndicatorFlag(true));
        
        const {
            reducerSetSeasonIndex:seasonIndex, 
            reducerSetActivityIndicatorFlag:activityIndicatorFlag,
        } = getState(); 

        let appParam = await initAppParamFromStorage();
        
        let rowCount = 0;
        let levelIndex = 0;
        let levelData = [];
        if(seasonIndex === FourSeasonDictionary.Spring){
            rowCount = 4;
            levelData = require('../../Asserts/Levels/4x4/DataRand_4x4.json');
            dispatch(createActionSetLevelData(levelData));
            if(appParam.hasOwnProperty('selectedSpringLevel')){
                levelIndex = appParam.selectedSpringLevel;
            }
            if(appParam.hasOwnProperty('springLevelParam')){
                if(appParam.springLevelParam.length < levelData.length){
                    for(let i=appParam.springLevelParam.length; i<levelData.length; i++){
                        appParam.springLevelParam[i] = 0;
                    }
                    await persistAppParamToStorage({springLevelParam: appParam.springLevelParam});
                }
            }
        }
        else if(seasonIndex === FourSeasonDictionary.Summer){
            rowCount = 5;
            levelData = require('../../Asserts/Levels/5x5/DataRand_5x5.json');
            dispatch(createActionSetLevelData(levelData));
            if(appParam.hasOwnProperty('selectedSummerLevel')){
                levelIndex = appParam.selectedSummerLevel;
            }
            if(appParam.hasOwnProperty('summerLevelParam')){
                if(appParam.summerLevelParam.length < levelData.length){
                    for(let i=appParam.summerLevelParam.length; i<levelData.length; i++){
                        appParam.summerLevelParam[i] = 0;
                    }
                    await persistAppParamToStorage({summerLevelParam: appParam.summerLevelParam});
                }
            }
        }
        else if(seasonIndex === FourSeasonDictionary.Autumn){
            rowCount = 6;
            levelData = require('../../Asserts/Levels/6x6/DataRand_6x6.json');
            dispatch(createActionSetLevelData(levelData));
            if(appParam.hasOwnProperty('selectedAutumnLevel')){
                levelIndex = appParam.selectedAutumnLevel;
            }
            if(appParam.hasOwnProperty('autumnLevelParam')){
                if(appParam.autumnLevelParam.length < levelData.length){
                    for(let i=appParam.autumnLevelParam.length; i<levelData.length; i++){
                        appParam.autumnLevelParam[i] = 0;
                    }
                    await persistAppParamToStorage({autumnLevelParam: appParam.autumnLevelParam});
                }
            }
        }
        else if(seasonIndex === FourSeasonDictionary.Winter){
            rowCount = 7;
            levelData = require('../../Asserts/Levels/7x7/DataRand_7x7.json');
            dispatch(createActionSetLevelData(levelData));
            if(appParam.hasOwnProperty('selectedWinterLevel')){
                levelIndex = appParam.selectedWinterLevel;
            }
            if(appParam.hasOwnProperty('winterLevelParam')){
                if(appParam.winterLevelParam.length < levelData.length){
                    for(let i=appParam.winterLevelParam.length; i<levelData.length; i++){
                        appParam.winterLevelParam[i] = 0;
                    }
                    await persistAppParamToStorage({winterLevelParam: appParam.winterLevelParam});
                }
            }
        }

        const columnCount = rowCount;
        dispatch(createActionSetPalletSize(rowCount, columnCount));
        dispatch(createActionSetLevelIndex(levelIndex));
        dispatch(createActionSetupLevel(levelIndex));
        //dispatch(createActionSetActivityIndicatorFlag(false));
    };
}

export function createActionSetupLevel(levelIndex){
    return (dispatch, getState)=>{
        const {
            reducerSetSeasonIndex:seasonIndex, 
            reducerSetPalletSize:palletSize,
            reducerSetLevelData:levelDataArray,
        } = getState(); 
        dispatch(createActionSetLevelIndex(levelIndex));

        var matrix = [];
        let leafBgColorArray = shuffle([...LeafBackgroundColorArray]);
        let leafPairCount = palletSize.rowCount - 1;
        let leafArray = [];
        let levelData = levelDataArray[levelIndex];
        let startEndNodeGridArray = [];

        for(var i=0; i<palletSize.rowCount; i++){
            var row = [];
            for(var j=0;j<palletSize.columnCount;j++){
                let rowIndex = i;
                let columnIndex = j;
                var grid = Object.assign({}, Grid, {rowIndex:rowIndex, columnIndex:columnIndex}); //new Grid(i, j);
                for(var k=0; k<levelData.length; k++){
                    if(levelData[k] == (i*palletSize.columnCount + j)){
                        var pairIndex = 0;
                        var needToAppendToStartEndArray = false;
                        if(k % 2 === 0){
                            pairIndex = levelData[k+1];
                            needToAppendToStartEndArray = true;
                        }
                        else{
                            pairIndex = levelData[k-1];
                        }
                        grid.pairRowIndex = (pairIndex - (pairIndex % palletSize.columnCount)) / palletSize.columnCount;
                        grid.pairColumnIndex = pairIndex % palletSize.columnCount;
                        grid.isBlocked = true;
                        if(needToAppendToStartEndArray){
                            startEndNodeGridArray.push({
                                startRow: grid.rowIndex,
                                startColumn: grid.columnIndex,
                                endRow: grid.pairRowIndex,
                                endColumn: grid.pairColumnIndex,
                            });
                        }
                    }
                }
                row.push(grid);
            }
            matrix.push(row);
        }
        let leafIndexSet = new Set();
        let leafBgColorSet = new Set();
        for(var i=0; i<startEndNodeGridArray.length; i++){
            let leafIndex = -1;
            let previousLeafIndexSetCount = leafIndexSet.size;
            while(leafIndexSet.size === previousLeafIndexSetCount){
                leafIndex = Math.floor(Math.random() * 21) + 1;
                leafIndexSet.add(leafIndex);
            }
            let previousLeafBgColorSetCount = leafBgColorSet.size;
            let leafBackgroundColor = '#FFFFFF';
            while(leafBgColorSet.size === previousLeafBgColorSetCount){
                leafBackgroundColor = LeafBackgroundColorArray[Math.floor(Math.random() * LeafBackgroundColorArray.length)];
                leafBgColorSet.add(leafBackgroundColor);
            }
            
            let startEndNodeGridPair = startEndNodeGridArray[i];
            matrix[startEndNodeGridPair.startRow][startEndNodeGridPair.startColumn].leafIndex = leafIndex;
            matrix[startEndNodeGridPair.startRow][startEndNodeGridPair.startColumn].leafBackgroundColor = leafBackgroundColor;
            matrix[startEndNodeGridPair.endRow][startEndNodeGridPair.endColumn].leafIndex = leafIndex;
            matrix[startEndNodeGridPair.endRow][startEndNodeGridPair.endColumn].leafBackgroundColor = leafBackgroundColor;
        }
        dispatch(createActionSetGridMatrix(matrix));
    };
}



export function createActionFindPath(){
    return (dispatch, getState)=>{
        console.log("[createActionFindPath]");
        const{
            reducerSetGridMatrix: gridMatrix,
            reducerSetPalletSize: palletSize,
            reducerSetStartGrid: startGrid,
            reducerSetEndGrid: endGrid,
        } = getState();
        // console.log(JSON.stringify(startGrid));
        // console.log(JSON.stringify(endGrid));
        // console.log(JSON.stringify(gridMatrix));
        if((startGrid.rowIndex>=0) && (startGrid.columnIndex>=0) && (endGrid.rowIndex>=0) && (endGrid.columnIndex>=0)){
            var matrix = [];
            for(var i=0; i<palletSize.rowCount; i++){
                var row = [];
                for(var j=0; j<palletSize.columnCount; j++){
                    row.push(Object.assign({}, gridMatrix[i][j]));
                }
                matrix.push(row);
            }
            let openNodeArray = new Array();
            let closedNodeArray = new Array();
            let startGridNode = matrix[startGrid.rowIndex][startGrid.columnIndex];
            let endGridNode = matrix[endGrid.rowIndex][endGrid.columnIndex];
            openNodeArray.push(startGridNode);
            let found = false;
            let completed = false;
            while((!found) && (!completed)){
                let currentGridNode = null; //current = node in OPEN with the lowest f_cost
                let lowestFCost = Number.POSITIVE_INFINITY;
                for(var i=0; i<openNodeArray.length; i++){
                    let thisNode = openNodeArray[i];
                    if((thisNode.parentRowIndex === null) || (thisNode.parentColumnIndex == null)){
                        thisNode.gCost = Math.abs(thisNode.rowIndex - startGrid.rowIndex) + Math.abs(thisNode.columnIndex - startGrid.columnIndex);
                        thisNode.hCost = Math.abs(thisNode.rowIndex - endGrid.rowIndex) + Math.abs(thisNode.columnIndex - endGrid.columnIndex);
                        thisNode.fCost = thisNode.gCost + thisNode.hCost;
                        //console.log("thisNode fCost: " + thisNode.fCost);
                    }
                    else{
                        thisNode.gCost = matrix[thisNode.parentRowIndex][thisNode.parentColumnIndex].gCost + 1;
                        thisNode.hCost = Math.abs(thisNode.rowIndex - endGrid.rowIndex) + Math.abs(thisNode.columnIndex - endGrid.columnIndex);
                        thisNode.fCost = thisNode.gCost + thisNode.hCost;
                        //console.log("thisNode fCost: " + thisNode.fCost);
                    }
                    if(thisNode.fCost < lowestFCost){
                        lowestFCost = thisNode.fCost;
                        currentGridNode = thisNode;
                        //console.log("lowestFCost");
                        //console.log(JSON.stringify(currentGridNode));
                    }
                }

                if(currentGridNode != null){
                    openNodeArray = removePathGrid(openNodeArray, currentGridNode);
                    closedNodeArray.push(currentGridNode);
                    //console.log("[CurrentGridNode]: "+JSON.stringify(currentGridNode));
                    //if(Object.is(currentGridNode, endGridNode)){
                    if((currentGridNode.rowIndex === endGridNode.rowIndex) && (currentGridNode.columnIndex === endGridNode.columnIndex)){
                        found = true;
                    }
                    else{
                        var neighbourGridNodeArray = [];
                        if((currentGridNode.rowIndex-1)>=0){
                            neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex-1][currentGridNode.columnIndex]);
                        }
                        if((currentGridNode.rowIndex+1)<palletSize.rowCount){
                            neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex+1][currentGridNode.columnIndex]);
                        }
                        if((currentGridNode.columnIndex-1)>=0){
                            neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex][currentGridNode.columnIndex-1]);
                        }
                        if((currentGridNode.columnIndex+1)<palletSize.columnCount){
                            neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex][currentGridNode.columnIndex+1]);
                        }
                        //console.log("[neighbourGridNodeArray length]: " + neighbourGridNodeArray.length);
                        for(var i=0; i<neighbourGridNodeArray.length; i++){
                            let neighbourGridNode = neighbourGridNodeArray[i];
                            //console.log("[neighbourGridNode]: " + JSON.stringify(neighbourGridNode));
                            if(neighbourGridNode.isBlocked || containsPathGrid(closedNodeArray, neighbourGridNode)){
                                continue;
                            }
                            // console.log("[currentGridNode.gCost]: "+currentGridNode.gCost);
                            // console.log("[neighbourGridNode.rowIndex]: "+neighbourGridNode.rowIndex);
                            // console.log("[endGrid.rowIndex]: "+endGrid.rowIndex);
                            // console.log("[neighbourGridNode.columnIndex]: "+neighbourGridNode.columnIndex);
                            // console.log("[endGrid.columnIndex]: "+endGrid.columnIndex);
                            let newGCost = currentGridNode.gCost + 1;
                            let newHCost = Math.abs(neighbourGridNode.rowIndex - endGrid.rowIndex) + Math.abs(neighbourGridNode.columnIndex - endGrid.columnIndex);
                            let newFCost = newGCost + newHCost;
                            // console.log('[newGCost]: '+ newGCost);
                            // console.log('[newHCost]: '+newHCost);
                            // console.log("[newFCost]: " + newFCost);
                            // console.log("[neighbourGridNode fCost]: " + neighbourGridNode.fCost);
                            if(neighbourGridNode.fCost == null){
                                neighbourGridNode.parentRowIndex = currentGridNode.rowIndex;
                                neighbourGridNode.parentColumnIndex = currentGridNode.columnIndex;
                                neighbourGridNode.gCost = newGCost;
                                neighbourGridNode.hCost = newHCost;
                                neighbourGridNode.fCost = newFCost;
                                console.log("[neighbourGridNode]: " + JSON.stringify(neighbourGridNode));
                            }
                            else if(neighbourGridNode.fCost > newFCost){
                                neighbourGridNode.parentRowIndex = currentGridNode.rowIndex;
                                neighbourGridNode.parentColumnIndex = currentGridNode.columnIndex;
                                neighbourGridNode.gCost = newGCost;
                                neighbourGridNode.hCost = newHCost;
                                neighbourGridNode.fCost = newFCost;
                                console.log("[neighbourGridNode]: " + JSON.stringify(neighbourGridNode));
                            }

                            if(!containsPathGrid(openNodeArray, neighbourGridNode)){
                                openNodeArray.push(neighbourGridNode);
                                //console.log('[openNodeArray]: '+JSON.stringify(openNodeArray));
                            }
                        }
                    }
                }
                else{
                    completed = true;
                }
                

                if(closedNodeArray.length >= (palletSize.rowCount * palletSize.columnCount)){
                    completed = true;
                }
            }

            if(found){
                console.log("[Found Path]");
                let done = false;
                let pathNode = endGridNode;
                while(!done){
                    console.log("[PathNode]: "+JSON.stringify(pathNode));
                    console.log("Path: [rowIndex:"+pathNode.rowIndex+", columnIndex:"+pathNode.columnIndex+"]");
                    //if(Object.is(pathNode, startGridNode))
                    if((pathNode.rowIndex === startGridNode.rowIndex) && (pathNode.columnIndex === startGridNode.columnIndex)){
                        done = true;
                    }
                    else{
                        pathNode = matrix[pathNode.parentRowIndex][pathNode.parentColumnIndex];
                        //if(!Object.is(pathNode, startGridNode)){
                        if((pathNode.rowIndex !== startGridNode.rowIndex) || (pathNode.columnIndex !== startGridNode.columnIndex)){
                            pathNode.isPath = true;
                        }
                    }
                }
                dispatch(createActionSetGridMatrix(matrix));
            }

        }
    };
}


const Grid = {
    rowIndex : -1,
    columnIndex : -1,
    parentRowIndex : null,
    parentColumnIndex : null,
    childRowIndex: null,
    childColumnIndex: null,
    gCost : null,
    hCost : null,
    fCost : null,
    isBlocked : false,
    pairRowIndex: null,
    pairColumnIndex: null,
    isPairFound:false,
    leafIndex:-1,
    leafBackgroundColor:'#F0F0F0',
};

function removePathGrid(nodeArray, gridNode) {
    for (var i = 0; i < nodeArray.length; i++) {
        if ((nodeArray[i].rowIndex === gridNode.rowIndex) && (nodeArray[i].columnIndex === gridNode.columnIndex)) {
            nodeArray.splice(i, 1);
            i--;
        }
    }
    return nodeArray;
};

function containsPathGrid(nodeArray, gridNode){
    for(var i = 0; i < nodeArray.length; i++){
        if((nodeArray[i].rowIndex === gridNode.rowIndex) && (nodeArray[i].columnIndex === gridNode.columnIndex)) {
            return true;
        }
    }
    return false;
};