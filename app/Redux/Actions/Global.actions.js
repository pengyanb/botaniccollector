export const ACTION_SET_ACTIVITY_INDICATOR_FLAG = "SetActivityIndicatorFlag";
export const ACTION_SET_SEASON_INDEX = "SetSeasonIndex";

export function createActionSetActivityIndicatorFlag(flag){
    return {type: ACTION_SET_ACTIVITY_INDICATOR_FLAG, flag: flag};
}

export function createActionSetSeasonIndex(index){
    return {type:ACTION_SET_SEASON_INDEX, index: index};
}