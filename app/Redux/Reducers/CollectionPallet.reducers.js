import {
    ACTION_SET_PALLET_SIZE,
    ACTION_SET_GRID_MATRIX,
    ACTION_FIND_PATH,
    ACTION_SET_LEVEL_DATA,
    ACTION_SET_LEVEL_INDEX,
} from '../Actions/CollectionPallet.actions';

export function reducerSetLevelData(state=[], action){
    switch(action.type){
        case ACTION_SET_LEVEL_DATA:
        let newLevelData = [];
        for(var i=0; i < action.levelData.length; i++){
            newLevelData.push([...action.levelData[i]]);
        }
        return newLevelData;
        default: return state;
    }
}

export function reducerSetLevelIndex(state=0, action){
    switch(action.type){
        case ACTION_SET_LEVEL_INDEX: return action.levelIndex;
        default: return state;
    }
}
export function reducerSetPalletSize(state={rowCount:4, columnCount:4}, action){
    switch(action.type){
        case ACTION_SET_PALLET_SIZE: return Object.assign({}, action.size);
        default: return state; 
    }
}
export function reducerSetGridMatrix(state=[], action){
    switch(action.type){
        case ACTION_SET_GRID_MATRIX: 
        let matrix = [];
        for(var i=0; i<action.matrix.length; i++)
        {
            let row = [];
            for(var j=0; j<action.matrix[i].length; j++){
                row.push(Object.assign({},action.matrix[i][j]));
            }
            matrix.push(row);
        }
        return matrix;
        default:return state;
    }
}


