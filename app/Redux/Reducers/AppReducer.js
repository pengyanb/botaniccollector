import {combineReducers} from 'redux';

import {
    reducerSetActivityIndicatorFlag,
    reducerSetSeasonIndex,
} from './Global.reducers';

import {
    reducerSetLevelData,
    reducerSetLevelIndex,
    reducerSetPalletSize,
    reducerSetGridMatrix,
} from './CollectionPallet.reducers';

const AppReducer = combineReducers({
    reducerSetActivityIndicatorFlag,
    reducerSetSeasonIndex,

    reducerSetLevelData,
    reducerSetLevelIndex,
    reducerSetPalletSize,
    reducerSetGridMatrix,
});

export default AppReducer;