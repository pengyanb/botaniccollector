import{
    ACTION_SET_ACTIVITY_INDICATOR_FLAG,
    ACTION_SET_SEASON_INDEX,
} from '../Actions/Global.actions';

export function reducerSetActivityIndicatorFlag(state=true, action){
    switch(action.type){
        case ACTION_SET_ACTIVITY_INDICATOR_FLAG: return action.flag;
        default: return state;
    }
}

export function reducerSetSeasonIndex(state=0, action){
    switch(action.type){
        case ACTION_SET_SEASON_INDEX: return action.index;
        default: return state;
    }
}
