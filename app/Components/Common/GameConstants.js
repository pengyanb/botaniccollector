export const ComponentZindex = {
    ActivityIndicator: 5000,
    BackgroundImage:0,
};

export const FourSeasonDictionary = {
    Spring: 0,
    Summer: 1,
    Autumn: 2,
    Winter: 3,

    seasonName: function(seasonIndex){
        switch(seasonIndex){
            case FourSeasonDictionary.Spring:return "Spring";
            case FourSeasonDictionary.Summer:return "Summer";
            case FourSeasonDictionary.Autumn:return "Autumn";
            case FourSeasonDictionary.Winter:return "Winter";
            default:return "Error";
        }
    }
}

export const LeafBackgroundColorArray = [
    '#FF8A80',  //red
    '#B388FF',  //Deep Purple
    '#8C9EFF',  //Indigo
    '#B9F6CA',  //green
    '#FFFF8D',  //yellow
    '#795548',  //brown
    '#9E9E9E',  //gray
    '#607D8B',  //Blue grey
    '#FFFFFF',  //white
    '#000000',  //black
];
export function shuffle(array){
    var i = 0, j = 0, temp = null;
    for(i = array.length - 1; i > 0; i -= 1){
        j = Math.floor(Math.random() * (i + 1));
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

