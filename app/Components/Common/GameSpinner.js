import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Animated,
} from 'react-native';

import {ComponentZindex} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class GameSpinner extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            leafSpinnerOpacityValue: new Animated.Value(0.8),
            leafIndex: 1,
        };
        this.textFontSize = 12;
    }
    componentDidMount(){
        this.startLeafAnimation();
        this.textFontSize = parseInt(Dimensions.get('window').width * 0.04);
    }
    startLeafAnimation(){
        Animated.sequence([
            Animated.timing(this.state.leafSpinnerOpacityValue, {
                toValue: 1,
                duration: 50,
            }),
            Animated.timing(this.state.leafSpinnerOpacityValue, {
                toValue: 0.8,
                duration: 50,
            })
        ]).start(()=>{
            //console.log("isActive: "+this.props.isActive);
            if(this.state.leafIndex >= 21){
                if(this.props.isActive)
                    this.setState({leafIndex: 1});
            }
            else{
                if(this.props.isActive)
                    this.setState({leafIndex: this.state.leafIndex + 1});
            }
            if(this.props.isActive)
                this.startLeafAnimation();
        });
    }
    render(){
        if (this.props.isActive){
            const leafName = "Leaf" + this.state.leafIndex;
            //console.log(leafName);
            return (
                <View style={styles.container}>
                    <Animated.Image 
                        style={[styles.leafSpinner, {opacity: this.state.leafSpinnerOpacityValue}]}
                        source={ImageDictionary.Leaves[leafName]}
                        >
                    </Animated.Image>
                    <Text style={{fontSize:this.textFontSize, backgroundColor:'rgba(0,0,0,0)'}}>Loading...</Text>
                </View>
            );
        }
        else{
            return null;
        };
    }
}

const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:0,
        left:0,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent:'center',
        alignItems:'center',
        zIndex: ComponentZindex.ActivityIndicator,
    },
    leafSpinner:{
        width: 60,
        height: 60,
    }
});