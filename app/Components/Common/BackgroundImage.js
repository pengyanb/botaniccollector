import React from 'react';
import {
    Dimensions,
    Image,
    StyleSheet,
} from 'react-native';

import {ComponentZindex, FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class BackgroundImage extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return(
                <Image source={ImageDictionary.SpringGUI.Background} style={styles.container}/>   
            );
        }
        else if (this.props.seasonIndex === FourSeasonDictionary.Summer){
            return(
                <Image source={ImageDictionary.SummerGUI.Background} style={styles.container}/>
            );
        }
        else if (this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return(
                <Image source={ImageDictionary.AutumnGUI.Background} style={styles.container}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return(
                <Image source={ImageDictionary.WinterGUI.Background} style={styles.container}/>
            );
        }
    }
}

const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:0,
        left:0,
        justifyContent:'center',
        alignItems:'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        zIndex:ComponentZindex.BackgroundImage,
    }
});
