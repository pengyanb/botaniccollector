import React from 'react';
import {
    Image,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class QuitButton extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return (
                <Image style={this.props.style} source={ImageDictionary.SpringGUI.QuitButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Summer){
            return (
                <Image style={this.props.style} source={ImageDictionary.SummerGUI.QuitButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return (
                <Image style={this.props.style} source={ImageDictionary.AutumnGUI.QuitButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return (
                <Image style={this.props.style} source={ImageDictionary.WinterGUI.QuitButton}/>
            );
        }
    }
}