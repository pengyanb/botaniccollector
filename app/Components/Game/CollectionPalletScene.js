import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Image,
    DeviceEventEmitter,
    Platform
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

import Svg, {
    Circle,
    Line,
} from 'react-native-svg';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

import {
    AdMobBanner,
    AdMobInterstitial,
} from 'react-native-admob';

import GameSpinner from '../Common/GameSpinner';
import BackGroundImage from '../Common/BackgroundImage';
import MenuRoundButton from '../Menu/MenuRoundButton';
import LeafNode from '../Game/LeafNode';
import PathNode from '../Game/PathNode';
import MenuArrowButton from '../Menu/MenuArrowButton';
import TopMenuContainer from './TopMenuContainer';
import ListButton from './ListButton';
import RefreshButton from './RefreshButton';
import QuitButton from './QuitButton';
import LevelResult from './LevelResult';

import {
    createActionSetActivityIndicatorFlag,
    createActionSetSeasonIndex,
} from '../../Redux/Actions/Global.actions';

import {
    createActionInitializeCollectionPallet,
    createActionSetGridMatrix,
    createActionSetBlockedGrid,
    createActionSetupLevel,
} from '../../Redux/Actions/CollectionPallet.actions';

import{
    pathFindingRoutineForNodePair,
} from '../../Utility/PathFinding.js';

import {
    persistAppParamToStorage,
    initAppParamFromStorage,
} from '../../Utility/PersistDataHelper.js';

import {SoundPickUp, SoundLevelClear} from '../../Asserts/Sounds/SoundDictionary';

const admobInterstitialUnitID = (Platform.OS == "ios") ? 'ca-app-pub-3199275288482759/7719659423' : 'ca-app-pub-3199275288482759/8983923023';
const admobBannerUnitID = (Platform.OS == "ios") ? 'ca-app-pub-3199275288482759/6242926228' : 'ca-app-pub-3199275288482759/7507189822';

// console.log("[admobInterstitialUnitID]: " + admobInterstitialUnitID);

AdMobInterstitial.setAdUnitID(admobInterstitialUnitID);

class CollectionPalletScene extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            needToInitializeCollectionPallet:true,
            lastTouchedRowIndex: null,
            lastTouchedColumnIndex: null,
            lastTouchedBgColor: null,
            completedPairs: 0,
            levelClearSoundPlayed:false,
            coverRatio:0,
            levelParam:0,
            showLevelList:false,
            levelListPageIndex:0,
        };
        this.appParam = null;
        this.eventHandlers = [];
        this.textFontSize = 12;
    }
    componentDidMount(){
        this.props.reduxDispatch.setActivityIndicatorFlag(true);
        if(this.state.needToInitializeCollectionPallet){
            this.setState({needToInitializeCollectionPallet:false});
            this.props.reduxDispatch.initializeCollectionPallet();
        }
        AdMobInterstitial.requestAd();
        let deviceEventHandler = DeviceEventEmitter.addListener('interstitialDidClose', ()=>{
            console.log("[interstitialDidClose]");
            AdMobInterstitial.requestAd((event)=>{
                console.log(JSON.stringify(event));
            });
        });
        this.eventHandlers.push(deviceEventHandler);
        this.textFontSize = parseInt(Dimensions.get('window').width * 0.04);
        setTimeout(()=> { 
            this.props.reduxDispatch.setActivityIndicatorFlag(false);
        }, 1000);
    }
    componentWillUnmount(){
        for(let i=0; i<this.eventHandlers.length; i++){
            this.eventHandlers[i].remove();
        }
        this.eventHandlers = [];
    }
    componentWillReceiveProps(nextProps){
        if(this.props.reduxState.seasonIndex !== nextProps.reduxState.seasonIndex){
            this.setState({needToInitializeCollectionPallet:true});
        }

        let matrix = [];
        for(var i=0; i<nextProps.reduxState.gridMatrix.length; i++){
            let row = [];
            for(var j=0; j<nextProps.reduxState.gridMatrix[i].length; j++){
                row.push({...(nextProps.reduxState.gridMatrix[i][j])});
            }
            matrix.push(row);
        }

        let overallGridCount = nextProps.reduxState.palletSize.rowCount * nextProps.reduxState.palletSize.columnCount;
        let coveredGridCount = 0;
        let completedPairs = 0;
        for(let i=0; i<matrix.length; i++){
            for(let j=0; j<matrix[i].length; j++){
                let grid = matrix[i][j];
                //count the cover rate
                if(grid.isBlocked){     
                    coveredGridCount += 1;
                }
                //count completed pairs
                if((grid.leafIndex >0) && (grid.childRowIndex !== null) && (grid.childColumnIndex !== null)){
                    let hasChild = true;
                    let childGrid = matrix[grid.childRowIndex][grid.childColumnIndex];
                    while(hasChild){
                        if(childGrid.leafIndex > 0){
                            completedPairs += 1;
                            hasChild = false;
                        }
                        else{
                            if((childGrid.childRowIndex !== null) && (childGrid.childColumnIndex !== null)){
                                childGrid = matrix[childGrid.childRowIndex][childGrid.childColumnIndex];
                            }
                            else{
                                hasChild = false;
                            }
                        }
                    }
                }
            }
        }
        this.setState({completedPairs:completedPairs});
        let coverRatio = parseInt(coveredGridCount / overallGridCount * 100); //.toFixed(2)
        if((parseInt(coverRatio)==100) && (completedPairs < (nextProps.reduxState.palletSize.rowCount-1))){
            coverRatio = 99;
        }
        this.setState({coverRatio: coverRatio});
    }
    async componentWillUpdate(){
        if(this.appParam == null){
            this.appParam  = await initAppParamFromStorage();
        }
        if(this.state.completedPairs === this.props.reduxState.palletSize.rowCount-1){
            if(!this.state.levelClearSoundPlayed){
                SoundLevelClear.play();
                this.setState({levelClearSoundPlayed: true});
                let appParam = await initAppParamFromStorage();
                this.appParam = appParam;
                if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                    if(appParam.hasOwnProperty('springLevelParam')){
                        let levelParam = (this.state.coverRatio < 75 ? 1 : this.state.coverRatio === 100 ? 3 : 2);
                        if(levelParam > appParam.springLevelParam[this.props.reduxState.levelIndex]){
                            appParam.springLevelParam[this.props.reduxState.levelIndex] = levelParam;
                            await persistAppParamToStorage({springLevelParam: appParam.springLevelParam});
                        }
                    }
                }
                else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                    if(appParam.hasOwnProperty('summerLevelParam')){
                        let levelParam = (this.state.coverRatio < 75 ? 1 : this.state.coverRatio === 100 ? 3 : 2);
                        if(levelParam > appParam.summerLevelParam[this.props.reduxState.levelIndex]){
                            appParam.summerLevelParam[this.props.reduxState.levelIndex] = levelParam;
                            await persistAppParamToStorage({summerLevelParam: appParam.summerLevelParam});
                        }
                    }
                }
                else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                    if(appParam.hasOwnProperty('autumnLevelParam')){
                        let levelParam = (this.state.coverRatio < 75 ? 1 : this.state.coverRatio === 100 ? 3 : 2);
                        if(levelParam > appParam.autumnLevelParam[this.props.reduxState.levelIndex]){
                            appParam.autumnLevelParam[this.props.reduxState.levelIndex] = levelParam;
                            await persistAppParamToStorage({autumnLevelParam: appParam.autumnLevelParam});
                        }
                    }
                }
                else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                    if(appParam.hasOwnProperty('winterLevelParam')){
                        let levelParam = (this.state.coverRatio < 75 ? 1 : this.state.coverRatio === 100 ? 3 : 2);
                        if(levelParam > appParam.winterLevelParam[this.props.reduxState.levelIndex]){
                            appParam.winterLevelParam[this.props.reduxState.levelIndex] = levelParam;
                            await persistAppParamToStorage({winterLevelParam: appParam.winterLevelParam});
                        }
                    }
                }

                AdMobInterstitial.isReady((isReady)=>{
                    console.log("[AdMobInterstitial Ready]: "+isReady);
                    if(isReady){
                        AdMobInterstitial.showAd();
                    }
                    else{
                        AdMobInterstitial.requestAd((event)=>{
                        console.log(JSON.stringify(event));
                        AdMobInterstitial.showAd();
                    });}
                });
            }
        }
    }
    async componentDidUpdate(prevProps){
        if(this.state.needToInitializeCollectionPallet){
            this.setState({needToInitializeCollectionPallet:false});
            this.props.reduxDispatch.initializeCollectionPallet();
        }
        if(this.props.reduxState.levelIndex !== prevProps.reduxState.levelIndex){
            let appParam = await initAppParamFromStorage();
            this.appParam = appParam;
            if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                if(appParam.hasOwnProperty('springLevelParam')){
                    this.setState({levelParam: appParam.springLevelParam[this.props.reduxState.levelIndex]});
                }
            }
            else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                if(appParam.hasOwnProperty('summerLevelParam')){
                    this.setState({levelParam: appParam.summerLevelParam[this.props.reduxState.levelIndex]});
                }
            }
            else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                if(appParam.hasOwnProperty('autumnLevelParam')){
                    this.setState({levelParam: appParam.autumnLevelParam[this.props.reduxState.levelIndex]});
                }
            }
            else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                if(appParam.hasOwnProperty('winterLevelParam')){
                    this.setState({levelParam: appParam.winterLevelParam[this.props.reduxState.levelIndex]});
                }
            }
        }
    }
    handleGameTouchEndEvent(evt){
        let tapLocationX = evt.nativeEvent.pageX;
        let tapLocationY = evt.nativeEvent.pageY;

        this.setState({lastTouchedRowIndex: null});
        this.setState({lastTouchedColumnIndex:null});
        this.setState({lastTouchedBgColor: null});
        if(this.state.showLevelList){
            SoundPickUp.play();
            for(var i=0; i<5; i++){
                for(var j=0; j<5; j++){
                    const rIndex = i;
                    const cIndex = j;
                    const refKey = 'r'+rIndex+'c'+cIndex;
                    //console.log('[refKey]: '+refKey);
                    this.refs[refKey].measure(async (ox, oy, width, height, px, py)=>{
                        if((tapLocationX > px) && (tapLocationX < (px + width)) && (tapLocationY > py) && (tapLocationY < (py + height))){
                            let levelIndex = this.state.levelListPageIndex * 25 + rIndex * 5 + cIndex;
                            if(levelIndex !== this.props.reduxState.levelIndex){
                                this.setState({completedPairs:0});
                                this.setState({levelClearSoundPlayed:false});
                                this.props.reduxDispatch.setupLevel(levelIndex);
                                if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                                    await persistAppParamToStorage({selectedSpringLevel:levelIndex});
                                }
                                else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                                    await persistAppParamToStorage({selectedSummerLevel:levelIndex});
                                }
                                else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                                    await persistAppParamToStorage({selectedAutumnLevel:levelIndex});
                                }
                                else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                                    await persistAppParamToStorage({selectedWinterLevel:levelIndex});
                                }
                                this.setState({showLevelList:false});
                            }
                            else{
                                this.setState({showLevelList:false});
                            }
                            
                        }
                    });
                }
            }
        }
    }
    handleGameTouchMoveEvent(evt, isStart){
        console.log("[Handle Game Touch Move Event]");
        if(this.state.showLevelList){
            return;
        }
        let tapLocationX = evt.nativeEvent.pageX;
        let tapLocationY = evt.nativeEvent.pageY;

        // console.log("identifier: "+ evt.nativeEvent.identifier);
        // console.log("locationX: "+ evt.nativeEvent.locationX);
        // console.log("locationY: "+ evt.nativeEvent.locationY);
        // console.log("pageX: "+ evt.nativeEvent.pageX);
        // console.log("pageY: "+ evt.nativeEvent.pageY);
        // console.log("target: "+ evt.nativeEvent.target);
        // console.log("[handleGameTouchMoveEvent] end");
        let matrix = [];
        for(var i=0; i<this.props.reduxState.palletSize.rowCount; i++){
            let row = [];
            for(var j=0; j<this.props.reduxState.palletSize.columnCount; j++){
                row.push({...(this.props.reduxState.gridMatrix[i][j])});
            }
            matrix.push(row);
        }
        //console.log(JSON.stringify(matrix));
        let foundTouchedGrid = false;
        for(var i=0; i<this.props.reduxState.palletSize.rowCount; i++){
            for(var j=0; j<this.props.reduxState.palletSize.columnCount; j++){
                const rIndex = i;
                const cIndex = j;
                const refKey = 'r'+rIndex+'c'+cIndex;
                this.refs[refKey].measure((ox, oy, width, height, px, py)=>{
                    if((tapLocationX > px) && (tapLocationX < (px + width)) && (tapLocationY > py) && (tapLocationY < (py + height))){
                        let grid = matrix[rIndex][cIndex];      //find touched grid
                        foundTouchedGrid = true;
                        if(isStart){                            //if this is the touch start
                            //if this is a Leaf grid or a grid has a parent
                            if((grid.leafIndex > 0) || ((grid.parentRowIndex !== null) && (grid.parentColumnIndex !== null))){
                                if(grid.leafIndex > 0){         //if grid is a leaf Node 
                                    let needToDecrementChainCount = false;
                                    if((grid.parentRowIndex !== null) && (grid.parentColumnIndex !== null)){ //if grid has parent, clear parents chain
                                        needToDecrementChainCount = true;
                                        matrix = this.clearGridParentChain(matrix, grid);
                                    }
                                    if((grid.childRowIndex !== null) && (grid.childColumnIndex !== null)){  //if grid has child, clear children chain 
                                        needToDecrementChainCount = true;
                                        matrix = this.clearGridChildrenChain(matrix, grid);
                                    }
                                    if(needToDecrementChainCount && grid.isPairFound){
                                        grid.isPairFound = false;
                                        matrix[grid.pairRowIndex][grid.pairColumnIndex].isPairFound = false;
                                        //this.setState({completedPairs:this.state.completedPairs-1});
                                    }
                                }
                                else{                           //else for normal non-leaf node
                                    if((grid.childRowIndex !== null) && (grid.childColumnIndex !== null)){  //if grid has child node, clear children chain
                                        matrix = this.clearGridChildrenChain(matrix, grid);
                                    }
                                }

                                SoundPickUp.play();
                                this.setState({lastTouchedRowIndex: rIndex});                   //set touch start grid coordinates
                                this.setState({lastTouchedColumnIndex:cIndex});
                                this.setState({lastTouchedBgColor: grid.leafBackgroundColor});
                                grid.isBlocked = true;                                          //mark grid as blocked
                                grid.childRowIndex = null;
                                grid.childColumnIndex = null;
                                //console.log(JSON.stringify(matrix));
                                //matrix = this.handleMatrixCleanUp(matrix);
                                //console.log("[setGridMatrix 2]: " + JSON.stringify(matrix));
                                this.props.reduxDispatch.setGridMatrix(matrix);
                            }
                        }
                        else{           //else this is a move event
                            //console.log("[Touch moved]: " + this.state.lastTouchedRowIndex + " " + this.state.lastTouchedColumnIndex);
                            if((this.state.lastTouchedColumnIndex !== null) && (this.state.lastTouchedRowIndex !== null)){
                                //console.log("[Touch moved last grid] row: " + this.state.lastTouchedRowIndex + " column: " + this.state.lastTouchedColumnIndex);
                                //if touch moved to a different grid
                                if((grid.rowIndex !== this.state.lastTouchedRowIndex) || (grid.columnIndex !== this.state.lastTouchedColumnIndex)){
                                    //console.log("[Touch move to a different grid detected]");
                                    let lastTouchedGrid = matrix[this.state.lastTouchedRowIndex][this.state.lastTouchedColumnIndex];
                                    if(grid.leafIndex > 0){     //if moved to a leaf grid node
                                        if(grid.leafBackgroundColor !== lastTouchedGrid.leafBackgroundColor){ //leaf grid node has different color
                                            return;
                                        }
                                        // if((grid.childRowIndex !== null) || (grid.childColumnIndex !== null)){
                                        //     console.log("[grid is leaf Node and child is null]");
                                        //     return;
                                        // }
                                    }
                                    if(lastTouchedGrid.leafIndex > 0){  //if moved to a leaf grid node previously
                                        //console.log("[lastTouchedGrid] is a leaf grid");
                                        //console.log(JSON.stringify(lastTouchedGrid));
                                        if((lastTouchedGrid.parentRowIndex !== null) || (lastTouchedGrid.parentColumnIndex !== null)){ //if leaf grid node already has a parent
                                            console.log("[lastTouchedGrid] is a leaf grid and parent is not null");
                                            this.setState({lastTouchedBgColor: null});
                                            this.setState({lastTouchedRowIndex:null});
                                            this.setState({lastTouchedColumnIndex:null});
                                            return;
                                        }
                                    }
                                   
                                    // let needToClosePath = false;
                                    // let closeLeafGrid = null;
                                    let needToSwapParentChild = false;
                                    let needPathFinding = false;
                                     //if moved to a blocked path with same color （isBlocked, not leaf Grid, has same Color）
                                    if((grid.isBlocked) && (grid.leafBackgroundColor === lastTouchedGrid.leafBackgroundColor)){
                                        //console.log("[Moved to a blocked grid with same color]");
                                        let gridParentLeafNode = this.findRootParentLeafGrid(matrix, grid);
                                        let lastTouchedGridParentLeafNode = this.findRootParentLeafGrid(matrix, lastTouchedGrid);
                                        // let gridChildLeafNode = this.findRootChildLeafGrid(matrix, grid);
                                        // let lastTouchedGridChildLeafNode = this.findRootChildLeafGrid(matrix, lastTouchedGrid);
                                        if((gridParentLeafNode !== null) && (lastTouchedGridParentLeafNode !== null)){ //if this touch and previous touch all have parent Grid
                                            //if two touch has the same leaf Parent
                                            //console.log("[Both grid has parents]");
                                            //console.log("gridParentNode: row:"+gridParentLeafNode.rowIndex + " column:"+gridParentLeafNode.columnIndex);
                                            //console.log("lastTouchedGridParentLeafNode: row:"+lastTouchedGridParentLeafNode.rowIndex+ " column:"+lastTouchedGridParentLeafNode.columnIndex);
                                            if((gridParentLeafNode.rowIndex === lastTouchedGridParentLeafNode.rowIndex) && (gridParentLeafNode.columnIndex === lastTouchedGridParentLeafNode.columnIndex)){
                                                //console.log("[Both grid has same parents]");
                                                if((grid.childRowIndex !== null) && (grid.childColumnIndex !== null)){ //if this grid has child, clear children chain
                                                    //console.log("[Clear Grid children chain]");
                                                    matrix = this.clearGridChildrenChain(matrix, grid);
                                                }
                                                this.setState({lastTouchedRowIndex:grid.rowIndex});
                                                this.setState({lastTouchedColumnIndex:grid.columnIndex});
                                                //console.log("[setGridMatrix 3]: " + JSON.stringify(matrix));
                                                this.props.reduxDispatch.setGridMatrix(matrix); 
                                                return;
                                            }
                                            else{   //two grid has different parent leaf node merge to path
                                                //console.log("[Both grid has different parents]");
                                                // matrix = this.clearGridChildrenChain(matrix, gridParentLeafNode);
                                                // needToClosePath = true;
                                                // closeLeafGrid = gridParentLeafNode;
                                                needToSwapParentChild = true;
                                            }
                                        }
                                    }

                                    //if lastTouchedGrid has a parentGrid                                  
                                    if(lastTouchedGrid.leafIndex > 0){
                                        needPathFinding = true;
                                    }
                                    else{
                                        if((lastTouchedGrid.parentRowIndex!== null) &&(lastTouchedGrid.parentColumnIndex !== null)){
                                            needPathFinding = true;
                                        }
                                    }
                                    if(needPathFinding){
                                        //run path finding algrithm between lastTouchedGrid and current grid
                                        let pathFindingResult = pathFindingRoutineForNodePair(lastTouchedGrid, grid, matrix, this.props.reduxState.palletSize.rowCount, this.props.reduxState.palletSize.columnCount);
                                        for(var pathIndex=0; pathIndex<pathFindingResult.length; pathIndex++){ //for each grid on the path
                                            let pathIndexInfo = pathFindingResult[pathIndex];
                                            if(pathIndex === 0){                    //the end node of the path
                                                if(pathIndexInfo.leafIndex <= 0){   //if it is a normal path node    
                                                    this.setState({lastTouchedRowIndex:rIndex});
                                                    this.setState({lastTouchedColumnIndex:cIndex});
                                                    if(needToSwapParentChild){
                                                        let initParent = null;
                                                        let initChild = matrix[pathIndexInfo.rowIndex][pathIndexInfo.columnIndex];
                                                        if(pathFindingResult.length > 1){
                                                            let initParentInfo = pathFindingResult[1];
                                                            initParent = matrix[initParentInfo.rowIndex][initParentInfo.columnIndex];
                                                        }
                                                        else{
                                                            initParent = lastTouchedGrid;
                                                        }
                                                        //console.log("[initParent]: " + JSON.stringify(initParent));
                                                        //console.log("[initChild]: " + JSON.stringify(initChild));
                                                        matrix = this.swapGridParentChildChain(matrix, initChild, initParent);
                                                        //console.log("[newMatrix]: " + JSON.stringify(matrix));
                                                        this.setState({lastTouchedRowIndex:null});
                                                        this.setState({lastTouchedColumnIndex:null});
                                                    }
                                                }
                                                else{                               //if it is leaf Node 
                                                    matrix[pathIndexInfo.rowIndex][pathIndexInfo.columnIndex].isPairFound = true;
                                                    matrix[pathIndexInfo.pairRowIndex][pathIndexInfo.pairColumnIndex].isPairFound = true;
                                                    this.setState({lastTouchedRowIndex:null});
                                                    this.setState({lastTouchedColumnIndex:null});
                                                    this.setState({lastTouchedBgColor:null});
                                                }
                                            }
                                            if((pathIndexInfo.parentRowIndex !== null) && (pathIndexInfo.parentColumnIndex !== null)){
                                                let pathGrid = matrix[pathIndexInfo.rowIndex][pathIndexInfo.columnIndex];
                                                if(!needToSwapParentChild){
                                                    if((pathGrid.isBlocked) ){
                                                        // console.log("[leafBackgroundColor] not matching");
                                                        if((pathGrid.parentRowIndex !== null) && (pathGrid.parentColumnIndex !== null)){
                                                            // console.log("[parent Not null]");
                                                            matrix = this.clearGridParentChain(matrix, pathGrid);
                                                        }
                                                        if((pathGrid.childRowIndex !== null) && (pathGrid.childColumnIndex !== null)){
                                                            matrix = this.clearGridChildrenChain(matrix, pathGrid);
                                                        }
                                                    }
                                                }
                                                
                                                pathGrid.isBlocked = true;
                                                pathGrid.parentRowIndex = pathIndexInfo.parentRowIndex;
                                                pathGrid.parentColumnIndex = pathIndexInfo.parentColumnIndex;
                                                matrix[pathGrid.parentRowIndex][pathGrid.parentColumnIndex].childRowIndex = pathGrid.rowIndex;
                                                matrix[pathGrid.parentRowIndex][pathGrid.parentColumnIndex].childColumnIndex = pathGrid.columnIndex;
                                                if(pathGrid.leafIndex <= 0){
                                                    pathGrid.leafBackgroundColor = this.state.lastTouchedBgColor; //matrix[pathGrid.parentRowIndex][pathGrid.parentColumnIndex].leafBackgroundColor;
                                                }
                                            }
                                        }
                                    }
                                    
                                    //matrix = this.handleMatrixCleanUp(matrix);
                                    //console.log("[Final Matrix]: " + JSON.stringify(matrix));
                                    //console.log("[setGridMatrix 1]: " + JSON.stringify(matrix));
                                    this.props.reduxDispatch.setGridMatrix(matrix);
                                }
                            }
                        }
                    }
                });
                if(foundTouchedGrid){
                    return;
                }
            }
        }
    }
    findRootParentLeafGrid(matrix, grid){
        if((grid.parentRowIndex !== null) && (grid.parentColumnIndex !== null)){
            let parentGrid = matrix[grid.parentRowIndex][grid.parentColumnIndex];
            let hasParent = true;
            while(hasParent){
                if(parentGrid.leafIndex > 0){
                    return parentGrid;
                }
                if((parentGrid.parentRowIndex !== null) && (parentGrid.parentColumnIndex !== null)){
                    parentGrid = matrix[parentGrid.parentRowIndex][parentGrid.parentColumnIndex];
                }
                else{
                    hasParent = false;
                }
            }
            return null;
        }
        else if(grid.leafIndex >=0){
            return grid;
        }
        else{
            return null;
        }
    }
    findRootChildLeafGrid(matrix, grid){
        if((grid.childRowIndex !== null) && (grid.childColumnIndex !== null)){
            let childGrid = matrix[grid.childRowIndex][grid.childColumnIndex];
            let hasChild = true;
            while(hasChild){
                if(childGrid.leafIndex > 0){
                    return childGrid;
                }
                if((childGrid.childRowIndex !== null) && (childGrid.childColumnIndex !== null)){
                    childGrid = matrix[childGrid.childRowIndex][childGrid.childColumnIndex];
                }
                else{
                    hasChild = false;
                }
            }
            return null;
        }
        else if(grid.leafIndex >=0){
            return grid;
        }
        else{
            return null;
        }
    }
    clearGridParentChain(matrix, grid){
        if((grid.parentRowIndex !== null) && (grid.parentColumnIndex !== null)){
            let hasParent = true;
            let parentRowIndex = grid.parentRowIndex;
            let parentColumnIndex = grid.parentColumnIndex;
            grid.parentRowIndex = null;
            grid.parentColumnIndex = null;
            if(grid.leafIndex <= 0){grid.isBlocked = false;}
            else if(grid.isPairFound){
                grid.isPairFound = false;
                matrix[grid.pairRowIndex][grid.pairColumnIndex].isPairFound = false;
            }
            while(hasParent){
                let parentGrid = matrix[parentRowIndex][parentColumnIndex];
                parentGrid.childRowIndex = null;
                parentGrid.childColumnIndex = null;
                if(parentGrid.leafIndex <= 0){parentGrid.isBlocked = false;}
                else if(parentGrid.isPairFound){
                    parentGrid.isPairFound = false;
                    matrix[parentGrid.pairRowIndex][parentGrid.pairColumnIndex].isPairFound = false;
                }
                if((parentGrid.parentRowIndex !== null) && (parentGrid.parentColumnIndex !== null)){
                    parentRowIndex = parentGrid.parentRowIndex;
                    parentColumnIndex = parentGrid.parentColumnIndex;
                    parentGrid.parentRowIndex = null;
                    parentGrid.parentColumnIndex = null;
                }
                else{
                    hasParent = false;
                }
            }
        }
        return matrix;
    }
    clearGridChildrenChain(matrix, grid){
        if((grid.childRowIndex !== null) && (grid.childColumnIndex !== null)){
            let hasChild = true;
            let childRowIndex = grid.childRowIndex;
            let childColumnIndex = grid.childColumnIndex;
            grid.childRowIndex = null;
            grid.childColumnIndex = null;
            // if(grid.leafIndex <= 0){grid.isBlocked = false;}
            // else if(grid.isPairFound){
            //     grid.isPairFound = false;
            //     matrix[grid.pairRowIndex][grid.pairColumnIndex].isPairFound = false;
            // }
            while(hasChild){
                let childGrid = matrix[childRowIndex][childColumnIndex];
                childGrid.parentRowIndex = null;
                childGrid.parentColumnIndex = null;
                if(childGrid.leafIndex <= 0){childGrid.isBlocked = false;}
                else if(childGrid.isPairFound){
                    childGrid.isPairFound = false;
                    matrix[childGrid.pairRowIndex][childGrid.pairColumnIndex].isPairFound = false;
                }
                if((childGrid.childRowIndex !== null) && (childGrid.childColumnIndex !== null)){
                    childRowIndex = childGrid.childRowIndex;
                    childColumnIndex = childGrid.childColumnIndex;
                    childGrid.childRowIndex = null;
                    childGrid.childColumnIndex = null;
                }
                else{
                    hasChild = false;
                }
            }
        }
        return matrix;
    }
    swapGridParentChildChain(matrix, initChildGrid, initParentGrid){
        if((initChildGrid.childRowIndex !== null) && (initChildGrid.childColumnIndex !==null)){
            //console.log("[swapGridParentChildChain]: - clearGridChildrenChain");
            matrix = clearGridChildrenChain(marix, initChildGrid);
        }

        if((initChildGrid.parentRowIndex !== null) && (initChildGrid.parentColumnIndex !== null)){
            let hasParent = true;
            let prevParent = matrix[initParentGrid.rowIndex][initParentGrid.columnIndex];
            let currentChild = matrix[initChildGrid.rowIndex][initChildGrid.columnIndex];
            while(hasParent){          
                //onsole.log("[swapGridParentChildChain]: - hasParent"); 
                let tempParentRow = null;
                let tempParentColumn = null;
                if((currentChild.parentRowIndex !== null) && (currentChild.parentColumnIndex !== null)){ //if current Grid has parent
                    tempParentRow = currentChild.parentRowIndex;
                    tempParentColumn = currentChild.parentColumnIndex;      //save this parent coordinates in a temp var
                }
                else{
                    hasParent = false;                                      //no more parent
                }
                
                currentChild.parentRowIndex = prevParent.rowIndex;          //point current's parent to previous parent
                currentChild.parentColumnIndex = prevParent.columnIndex;
                prevParent.childRowIndex = currentChild.rowIndex;           //point previous parent's child to current grid
                prevParent.childColumnIndex = currentChild.columnIndex;

                //console.log("[currentChild]: " + JSON.stringify(currentChild));
                //console.log("[prevParent]: " + JSON.stringify(prevParent));

                if(hasParent){                                              //if has Parent
                    prevParent = currentChild;                              //current Grid became previous Parent for next loop
                    currentChild = matrix[tempParentRow][tempParentColumn]; //temp Parent saved before become currentChild for next loop
                }
                else{
                    currentChild.childRowIndex = null;
                    currentChild.childColumnIndex = null;
                    if(currentChild.leafIndex > 0){
                        currentChild.isPairFound = true;
                        matrix[currentChild.pairRowIndex][currentChild.pairColumnIndex].isPairFound = true;
                    }
                }
            }
        }
        else{   //else initial child grid has no parent grid
            matrix[initChildGrid.rowIndex][initChildGrid.columnIndex].parentRowIndex = initParentGrid.rowIndex;
            matrix[initChildGrid.rowIndex][initChildGrid.columnIndex].parentColumnIndex = initParentGrid.columnIndex;
            matrix[initParentGrid.rowIndex][initParentGrid.columnIndex].childRowIndex = initChildGrid.rowIndex;
            matrix[initParentGrid.rowIndex][initParentGrid.columnIndex].childColumnIndex = initChildGrid.columnIndex;
        }
        return matrix;
    }
    render(){
        let gridWidth = Dimensions.get('window').width * 0.9 / this.props.reduxState.palletSize.columnCount;
        let gridHeigh = Dimensions.get('window').height * 0.6 / this.props.reduxState.palletSize.rowCount;
        let unitSize = gridWidth < gridHeigh ? (gridWidth-2) : (gridHeigh-2);
        const LineWidth = unitSize * 0.2;
        const palletSizeRowCount = this.props.reduxState.palletSize.rowCount;
        const textHeight = Dimensions.get('window').height*0.075;
        let listPageMatrix = [];
        if(this.state.showLevelList){
            gridWidth = Dimensions.get('window').width * 0.9 / 5;
            gridHeigh = Dimensions.get('window').height * 0.6 / 5;
            unitSize = gridWidth < gridHeigh ? (gridWidth-2) : (gridHeigh-2);

            let startIndex = this.state.levelListPageIndex * 25;
            let appParam = this.appParam;
            //console.log('[appParm]: ' + JSON.stringify(appParam));
            let levelParamList = [];
            if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                if(appParam.hasOwnProperty('springLevelParam')){levelParamList = appParam.springLevelParam;}
            }
            else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                if(appParam.hasOwnProperty('summerLevelParam')){levelParamList = appParam.summerLevelParam;}
            }
            else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                if(appParam.hasOwnProperty('autumnLevelParam')){levelParamList = appParam.autumnLevelParam;}
            }
            else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                if(appParam.hasOwnProperty('winterLevelParam')){levelParamList = appParam.winterLevelParam;}
            }       
            for(let i=0; i<5; i++){
                let row = [];
                for(let j=0; j<5; j++){
                    if((startIndex + i * 5 + j)< levelParamList.length){
                        let levelIndex = startIndex + i * 5 +j;
                        row.push({
                            levelIndex: levelIndex,
                            levelParam: levelParamList[levelIndex]
                        });
                    }
                    else{
                        row.push({});
                    }
                }
                listPageMatrix.push(row);
            }
            console.log("[listPageMatrix]: "+JSON.stringify(listPageMatrix));
        }
        return(
            <View style={styles.container}>
                {/***************************[Game Spinner]*******************************/}
                <GameSpinner isActive={this.props.reduxState.activityIndicatorFlag}/>
                {/***************************[BackgroundImage]*******************************/}
                {this.props.reduxState.activityIndicatorFlag ? null :
                    (<BackGroundImage seasonIndex={this.props.reduxState.seasonIndex}/>)
                }
                {/***************************[Menu Section]*******************************/}
                {this.props.reduxState.activityIndicatorFlag ? null : (
                    <View style={{width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.25, backgroundColor:'rgba(0,0,0,0)', flexDirection:'column', justifyContent:'flex-end', alignItems:'center'}}>
                        <TopMenuContainer seasonIndex={this.props.reduxState.seasonIndex} style={{position:'absolute', top:0, left:0, resizeMode:'stretch', width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.2}}/>
                        <Text style={{fontSize:this.textFontSize, paddingTop:textHeight*0.4, paddingLeft:textHeight, position:'absolute', left:0, top:0, textAlign:'left', width:Dimensions.get('window').width*0.5, height:textHeight}}>
                            Level: {this.props.reduxState.levelIndex+1}
                        </Text>
                        <Text style={{fontSize:this.textFontSize, paddingTop:textHeight*0.4, paddingRight:textHeight, position:'absolute', right:0, top:0, textAlign:'right', width:Dimensions.get('window').width*0.5, height:textHeight}}>
                            {''+palletSizeRowCount+'x'+palletSizeRowCount}
                        </Text>
                        <Text style={{fontSize:this.textFontSize, paddingTop:textHeight*0.4, paddingLeft:textHeight, position:'absolute', left:0, top:textHeight, textAlign:'left', width:Dimensions.get('window').width*0.5, height:textHeight}}>
                            Pairs: {this.state.completedPairs}
                        </Text>
                        <Text style={{fontSize:this.textFontSize, paddingTop:textHeight*0.4, paddingRight:textHeight, position:'absolute', right:0, top:textHeight,textAlign:'right', width:Dimensions.get('window').width*0.5, height:textHeight}}>
                            Cover Ratio: {this.state.coverRatio}%
                        </Text>

                        {this.state.levelParam > 0 ? (
                            <View style={{position:'absolute', top:Dimensions.get('window').height*0.05, left:Dimensions.get('window').width*0.35, height:Dimensions.get('window').width*0.1, flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                                <Image style={{width:Dimensions.get('window').width * 0.1, height:Dimensions.get('window').width*0.1, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf23}/>
                                {this.state.levelParam > 1 ? (<Image style={{width:Dimensions.get('window').width*0.1,height:Dimensions.get('window').width*0.1, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf23}/>) : (<Image style={{width:Dimensions.get('window').width*0.1, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf22}/>)}
                                {this.state.levelParam > 2 ? (<Image style={{width:Dimensions.get('window').width*0.1,height:Dimensions.get('window').width*0.1, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf23}/>) : (<Image style={{width:Dimensions.get('window').width*0.1, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf22}/>)}
                            </View>
                        ):null}
                        
                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-around', width:Dimensions.get('window').width, height:Dimensions.get('window').width*0.1}}>
                            {this.props.reduxState.levelIndex !== 0 ? (<TouchableOpacity style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                onPress={async ()=>{
                                    SoundPickUp.play();
                                    if(this.state.showLevelList){
                                        console.log("[levelListPageIndex]: " + this.state.levelListPageIndex);
                                        if((this.state.levelListPageIndex - 1) >= 0){
                                            this.setState({levelListPageIndex: this.state.levelListPageIndex - 1});
                                        }
                                    }
                                    else{
                                        this.setState({completedPairs:0});
                                        this.setState({levelClearSoundPlayed:false});
                                        this.props.reduxDispatch.setupLevel(this.props.reduxState.levelIndex - 1);
                                        if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                                            await persistAppParamToStorage({selectedSpringLevel:this.props.reduxState.levelIndex - 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                                            await persistAppParamToStorage({selectedSummerLevel:this.props.reduxState.levelIndex - 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                                            await persistAppParamToStorage({selectedAutumnLevel:this.props.reduxState.levelIndex - 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                                            await persistAppParamToStorage({selectedWinterLevel:this.props.reduxState.levelIndex - 1});
                                        }
                                    }
                                }}>
                                <MenuArrowButton style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}} 
                                                seasonIndex={this.props.reduxState.seasonIndex} 
                                                isLeft={true}
                                                buttonText={" "}/>
                            </TouchableOpacity>):(<View style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}></View>)}
                            <TouchableOpacity style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    if(!this.state.showLevelList){  //going to show Level List
                                        let levelListPageIndex = (this.props.reduxState.levelIndex - (this.props.reduxState.levelIndex % 25)) / 25;
                                        this.setState({levelListPageIndex: levelListPageIndex});
                                    }
                                    this.setState({showLevelList: !this.state.showLevelList});
                                }}>
                                <ListButton style={{resizeMode:'contain', width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                                seasonIndex={this.props.reduxState.seasonIndex}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    this.setState({completedPairs:0});
                                    this.setState({levelClearSoundPlayed:false});
                                    this.setState({showLevelList:false});
                                    this.props.reduxDispatch.setupLevel(this.props.reduxState.levelIndex);
                                }}>
                                <RefreshButton style={{resizeMode:'contain', width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                                seasonIndex={this.props.reduxState.seasonIndex}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    Actions.MenuScene();
                                }}>
                                <QuitButton style={{resizeMode:'contain', width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                                seasonIndex={this.props.reduxState.seasonIndex}/>
                            </TouchableOpacity>
                            {this.props.reduxState.levelIndex !== (this.props.reduxState.levelData.length-1) ?(<TouchableOpacity style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}
                                onPress={async ()=>{
                                    SoundPickUp.play();
                                    if(this.state.showLevelList){
                                        let maxPageIndex = ((this.props.reduxState.levelData.length - (this.props.reduxState.levelData.length % 25)) / 25)
                                                            + ((this.props.reduxState.levelData.length % 25)===0?0:1);
                                        console.log("[maxPageIndex]: "+ maxPageIndex);
                                        console.log("[levelListPageIndex]: " + this.state.levelListPageIndex);
                                        if((this.state.levelListPageIndex + 1) < maxPageIndex){
                                            this.setState({levelListPageIndex: this.state.levelListPageIndex + 1});
                                        }
                                    }
                                    else{
                                        this.setState({completedPairs:0});
                                        this.setState({levelClearSoundPlayed:false});
                                        this.props.reduxDispatch.setupLevel(this.props.reduxState.levelIndex + 1);
                                        if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                                            await persistAppParamToStorage({selectedSpringLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                                            await persistAppParamToStorage({selectedSummerLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                                            await persistAppParamToStorage({selectedAutumnLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                                            await persistAppParamToStorage({selectedWinterLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                    }
                                }}>
                                <MenuArrowButton style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}} 
                                                seasonIndex={this.props.reduxState.seasonIndex} 
                                                isLeft={false}
                                                buttonText={" "}/>
                            </TouchableOpacity>):(<View style={{width:Dimensions.get('window').width*0.2, height:Dimensions.get('window').width*0.1}}></View>)}
                        </View>
                    </View>
                )}
                {/***************************[Game Play Section]*******************************/}
                {this.props.reduxState.activityIndicatorFlag ? null : 
                    this.state.showLevelList ? (
                    <View style={[styles.container, {justifyContent:'center', width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.65, backgroundColor:'rgba(0,0,0,0)'}]}
                            onStartShouldSetResponder={(evt)=>{return true;}}
                            onMoveShouldSetResponder={(evt)=>{return false}}
                            onStartShouldSetResponderCapture={(evt)=>{return true;}}
                            onMoveShouldSetResponderCapture={(evt)=>{return false;}}
                            ref={"gameTouchContainer"}
                            onResponderRelease={(evt)=>{
                                this.handleGameTouchEndEvent(evt);
                            }}>
                            {listPageMatrix.length > 0 ?(listPageMatrix.map((row, rIndex)=>{
                                return (<View key={"r"+rIndex} style={{flexDirection:'row', justifyContent:'center', alignItems:'center',width:Dimensions.get('window').width*0.9}}>
                                            {row.map((grid,cIndex)=>{
                                                const refKey = "r" + rIndex + "c"+cIndex;
                                                const gridColor = ((typeof grid.levelParam === typeof undefined) || (grid.levelParam === 0)) ?  'rgba(120,144,156,0.5)':'rgba(185,246,202, 0.9)';
                                                const levelIndexText = ((typeof grid.levelIndex === typeof undefined) ? ' ' : (' ' + (grid.levelIndex + 1)) );
                                                return (<View key={"c"+cIndex} style={{width:unitSize+2,height:unitSize+2, flexDirection:'column', padding:1, justifyContent:'center', alignItems:'center'}}>
                                                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', width:unitSize, height:unitSize, backgroundColor:gridColor}}
                                                                ref={refKey}>
                                                                {grid.levelIndex===this.props.reduxState.levelIndex?(<Svg height={unitSize} width={unitSize} style={{position:'absolute',top:0,left:0}}>
                                                                    <Circle cx={unitSize*0.5}
                                                                            cy={unitSize*0.5}
                                                                            r={unitSize*0.4}
                                                                            fill={'#FFFFFF'}
                                                                    />
                                                                </Svg>
                                                                ):null}
                                                                <Text style={{fontSize:this.textFontSize}}>{levelIndexText}</Text>
                                                            </View>
                                                        </View>);
                                            })}
                                        </View>);
                            })) : null}
                    </View>
                    ) : (
                    <View style={[styles.container, {justifyContent:'center', width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.65, backgroundColor:'rgba(0,0,0,0)'}]}
                            onStartShouldSetResponder={(evt)=>{return true;}}
                            onMoveShouldSetResponder={(evt)=>{return true;}}
                            onStartShouldSetResponderCapture={(evt)=>{return true;}}
                            onMoveShouldSetResponderCapture={(evt)=>{return true;}}
                            ref={"gameTouchContainer"}
                            onResponderStart={(evt)=>{
                                if(this.state.completedPairs !== (this.props.reduxState.palletSize.rowCount - 1)){
                                    this.handleGameTouchMoveEvent(evt, true);
                                }
                            }}
                            onResponderMove={(evt)=>{
                                if(this.state.completedPairs !== (this.props.reduxState.palletSize.rowCount - 1)){
                                    this.handleGameTouchMoveEvent(evt, false);
                                }
                            }}
                            onResponderRelease={(evt)=>{
                                this.handleGameTouchEndEvent(evt);
                            }}
                        >
                        {this.props.reduxState.gridMatrix.map((row, rIndex)=>{
                            return (<View key={"r"+rIndex} style={{flexDirection:'row', justifyContent:'center', alignItems:'center',width:Dimensions.get('window').width*0.9}}>
                                        {row.map((grid, cIndex)=>{
                                            const refKey = "r"+rIndex+"c"+cIndex;
                                            const gridOpacity = grid.isBlocked ? 0.9 : 0.5;
                                            return (<View key={"c"+cIndex} style={{width:unitSize+2,height:unitSize+2,flexDirection:'column', padding:1, justifyContent:'center', alignItems:'center'}}>
                                                        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', width:unitSize, height:unitSize, backgroundColor:'rgba(120,144,156, '+gridOpacity+')'}}
                                                            ref={refKey}>
                                                            {grid.leafIndex>0?(<Svg height={unitSize} width={unitSize} style={{position:'absolute',top:0,left:0}}>
                                                                    <Circle cx={unitSize*0.5}
                                                                            cy={unitSize*0.5}
                                                                            r={unitSize*0.4}
                                                                            fill={grid.leafBackgroundColor}
                                                                    />
                                                                </Svg>
                                                            ):null}
                                                            <PathNode 
                                                                isBlocked={grid.isBlocked}
                                                                rowIndex={grid.rowIndex}
                                                                columnIndex={grid.columnIndex}
                                                                childRowIndex={grid.childRowIndex}
                                                                childColumnIndex={grid.childColumnIndex}
                                                                parentRowIndex={grid.parentRowIndex}
                                                                parentColumnIndex={grid.parentColumnIndex}
                                                                leafBackgroundColor={grid.leafBackgroundColor}
                                                                unitSize={unitSize}/>
                                                            {grid.leafIndex>0?(<LeafNode style={{width:unitSize*0.6, height:unitSize*0.6}} leafIndex={grid.leafIndex}/>):null}
                                                        </View>
                                                    </View>)
                                        })}
                                    </View>)
                        })}
                    </View>
                )}
                {/***************************[Bottom Padding Section]*******************************/}
                {this.props.reduxState.activityIndicatorFlag ? null : (
                    <AdMobBanner 
                        bannerSize="smartBannerPortrait"
                        adUnitID={admobBannerUnitID} 
                        didFailToReceiveAdWithError={(error)=>{
                            console.log("[AdMob]: "+JSON.stringify(error));
                    }}/>
                )}
                {/***************************[Game Completion Info]*******************************/}
                {(this.state.completedPairs === (this.props.reduxState.palletSize.rowCount-1)) ? (
                    <View style={{position:'absolute', left:Dimensions.get('window').width*0.1, top:Dimensions.get('window').height*0.4, width: Dimensions.get('window').width*0.8, height: Dimensions.get('window').height*0.3, backgroundColor:'rgba(0,0,0,0)', flexDirection:'column', justifyContent:'flex-end', alignItems:'center'}}>
                        <LevelResult seasonIndex={this.props.reduxState.seasonIndex} style={{position:'absolute', left:0, top:0, width: Dimensions.get('window').width*0.8, height: Dimensions.get('window').height*0.4, resizeMode:'contain'}}/>
                        <View style={{height:Dimensions.get('window').width*0.15, flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                            <Image style={{width:Dimensions.get('window').width*0.15, height:Dimensions.get('window').width*0.15, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf23}/>
                            {this.state.coverRatio < 75 ? (<Image style={{width:Dimensions.get('window').width*0.15,height:Dimensions.get('window').width*0.15, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf22}/>) : (<Image style={{width:Dimensions.get('window').width*0.15, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf23}/>)}
                            {this.state.coverRatio == 100 ? (<Image style={{width:Dimensions.get('window').width*0.15,height:Dimensions.get('window').width*0.15, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf23}/>) : (<Image style={{width:Dimensions.get('window').width*0.15, resizeMode:'contain'}} source={ImageDictionary.Leaves.Leaf22}/>)}
                        </View>
                        <View style={{height:Dimensions.get('window').width*0.15, flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                            <TouchableOpacity style={{width:Dimensions.get('window').width*0.15,height:Dimensions.get('window').width*0.1}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    this.setState({completedPairs:0});
                                    this.setState({levelClearSoundPlayed:false});
                                    this.props.reduxDispatch.setupLevel(this.props.reduxState.levelIndex);
                                }}>
                                <MenuRoundButton style={{width:Dimensions.get('window').width*0.15, height:Dimensions.get('window').width*0.1, resizeMode:'contain'}} seasonIndex={this.props.reduxState.seasonIndex} buttonText={"Retry"}/>
                            </TouchableOpacity>
                            <TouchableOpacity style={{width:Dimensions.get('window').width*0.15, height:Dimensions.get('window').width*0.1}}
                                onPress={async ()=>{
                                    if(this.props.reduxState.levelIndex < (this.props.reduxState.levelData.length-1)){
                                        SoundPickUp.play();
                                        this.setState({completedPairs:0});
                                        this.setState({levelClearSoundPlayed:false});
                                        this.props.reduxDispatch.setupLevel(this.props.reduxState.levelIndex + 1);
                                        if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Spring){
                                            await persistAppParamToStorage({selectedSpringLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Summer){
                                            await persistAppParamToStorage({selectedSummerLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Autumn){
                                            await persistAppParamToStorage({selectedAutumnLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                        else if(this.props.reduxState.seasonIndex === FourSeasonDictionary.Winter){
                                            await persistAppParamToStorage({selectedWinterLevel:this.props.reduxState.levelIndex + 1});
                                        }
                                    }
                                }}>
                                <MenuRoundButton style={{width:Dimensions.get('window').width*0.15, height:Dimensions.get('window').width*0.1, resizeMode:'contain'}} seasonIndex={this.props.reduxState.seasonIndex} buttonText={"Next"}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                ):null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        height:Dimensions.get('window').height,
        width:Dimensions.get('window').width,
        flexDirection:'column',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'#F5FCFF',
    }
});

const mapStateToProps = (state)=>{
    //console.log(JSON.stringify(state.reducerSetGridMatrix));
    return {reduxState:{
        activityIndicatorFlag: state.reducerSetActivityIndicatorFlag,
        seasonIndex: state.reducerSetSeasonIndex,
        palletSize: state.reducerSetPalletSize,
        gridMatrix: state.reducerSetGridMatrix,
        levelIndex: state.reducerSetLevelIndex,
        levelData: state.reducerSetLevelData,
    }};
};

const mapDispatchToProps = (dispatch)=>{
    return {reduxDispatch:{
        setActivityIndicatorFlag: (flag)=>{dispatch(createActionSetActivityIndicatorFlag(flag))},
        setSeasonIndex: (index)=>{dispatch(createActionSetSeasonIndex(index))},
        initializeCollectionPallet:()=>{dispatch(createActionInitializeCollectionPallet())},
        setGridMatrix:(matrix)=>{dispatch(createActionSetGridMatrix(matrix))},
        setBlockedGrid: (rowIndex, columnIndex, isSet)=>{dispatch(createActionSetBlockedGrid(rowIndex, columnIndex, isSet))},
        setupLevel:(newLevel)=>{dispatch(createActionSetupLevel(newLevel))},
    }};
};
export default connect(mapStateToProps, mapDispatchToProps, null, {withRef: true})(CollectionPalletScene);
