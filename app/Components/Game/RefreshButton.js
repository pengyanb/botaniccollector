import React from 'react';
import {
    Image,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class RefreshButton extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return (
                <Image style={this.props.style} source={ImageDictionary.SpringGUI.RefreshButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Summer){
            return (
                <Image style={this.props.style} source={ImageDictionary.SummerGUI.RefreshButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return (
                <Image style={this.props.style} source={ImageDictionary.AutumnGUI.RefreshButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return (
                <Image style={this.props.style} source={ImageDictionary.WinterGUI.RefreshButton}/>
            );
        }
    }
}