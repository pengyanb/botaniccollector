import React from 'react';
import {
    Image,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class ListButton extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return (
                <Image style={this.props.style} source={ImageDictionary.SpringGUI.ListButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Summer){
            return (
                <Image style={this.props.style} source={ImageDictionary.SummerGUI.ListButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return (
                <Image style={this.props.style} source={ImageDictionary.AutumnGUI.ListButton}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return (
                <Image style={this.props.style} source={ImageDictionary.WinterGUI.ListButton}/>
            );
        }
    }
}