import React from 'react';
import {
    Image,
    StyleSheet,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class TopMenuContainer extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return (
                <Image style={this.props.style} source={ImageDictionary.SpringGUI.TopMenuContainer}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Summer){
            return (
                <Image style={this.props.style} source={ImageDictionary.SummerGUI.TopMenuContainer}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return (
                <Image style={this.props.style} source={ImageDictionary.AutumnGUI.TopMenuContainer}/>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return (
                <Image style={this.props.style} source={ImageDictionary.WinterGUI.TopMenuContainer}/>
            );
        }
    }
}