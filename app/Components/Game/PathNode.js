import React from 'react';
import {
    View
} from 'react-native';

export default class PathNode extends React.Component{
    render(){
        if(this.props.isBlocked){
            if(((this.props.childRowIndex !== null) && (this.props.childColumnIndex !== null)) ||
                ((this.props.parentRowIndex !== null)&&(this.props.parentColumnIndex !== null)) ){
 
                let cTop=0, cLeft=0;
                let pTop=0, pLeft=0;
                let cWidth=0, cHeight=0;
                let pWidth=0, pHeight=0;
                let showLineToChild = false;
                let showLineToParent = false;
                if((this.props.childRowIndex !== null) && (this.props.childColumnIndex !== null)){ //has child
                    if(this.props.rowIndex === this.props.childRowIndex){ //if this grid is on the same row with child
                        //horizontal line
                        cWidth=(this.props.unitSize+4)*0.6 + 2;
                        cHeight=(this.props.unitSize+4)*0.2;
                        cTop = (this.props.unitSize+4)*0.4;
                        cLeft = (this.props.columnIndex < this.props.childColumnIndex)?((this.props.unitSize+4)*0.4):(-2);

                    }
                    else if(this.props.columnIndex === this.props.childColumnIndex){  //if this grid is on the same column with child
                        //Vertical line
                        cWidth=(this.props.unitSize+4)*0.2;
                        cHeight=(this.props.unitSize+4)*0.6 + 2;
                        cTop = (this.props.rowIndex < this.props.childRowIndex)?((this.props.unitSize+4)*0.4):(-2);
                        cLeft = (this.props.unitSize+4)*0.4; 
                    }
                    showLineToChild = true;
                }
                if((this.props.parentRowIndex !== null)&&(this.props.parentColumnIndex !== null)){ //has parent
                    if(this.props.rowIndex === this.props.parentRowIndex){    //if this grid is on the same row with parent
                        //horizontal line
                        pWidth=(this.props.unitSize+4)*0.6 + 2;
                        pHeight=(this.props.unitSize+4)*0.2;
                        pTop = (this.props.unitSize+4)*0.4;
                        pLeft = (this.props.columnIndex < this.props.parentColumnIndex)?((this.props.unitSize+4)*0.4):(-2);
                    }
                    else if(this.props.columnIndex === this.props.parentColumnIndex){
                        //vertical line
                        pWidth=(this.props.unitSize+4)*0.2;
                        pHeight=(this.props.unitSize+4)*0.6 + 2;
                        pTop = (this.props.rowIndex < this.props.parentRowIndex)?((this.props.unitSize+4)*0.4):(-2);
                        pLeft = (this.props.unitSize+4)*0.4; 
                    }
                    showLineToParent = true;
                }
                //console.log("[lineToChild]: cTop:"+cTop+" cLeft:"+cLeft+" cWidth:"+cWidth+" cHeight:"+cHeight);
                //console.log("[lineToParent]: pTop:"+pTop+" pLeft:"+pLeft+" pWidth:"+cWidth+" pHeight:"+pHeight);
                return (
                        <View style={{width:this.props.unitSize+4, height:this.props.unitSize+4, backgroundColor:'rgba(0,0,0,0)', position:'absolute', top:-2, left:-2}}>
                            {showLineToChild ? (
                                <View
                                    style={{
                                        flex:0,
                                        position:'absolute',
                                        borderRadius:2,
                                        left:cLeft,
                                        top:cTop,
                                        width:cWidth,
                                        height:cHeight,
                                        backgroundColor:this.props.leafBackgroundColor,
                                    }}
                                ></View>
                            ):null}
                            {showLineToParent ? (
                                <View
                                    style={{
                                        flex:0,
                                        position:'absolute',
                                        borderRadius:2,
                                        left:pLeft,
                                        top:pTop,
                                        width:pWidth,
                                        height:pHeight,
                                        backgroundColor:this.props.leafBackgroundColor,
                                    }}
                                ></View>
                            ):null}
                        </View>
                );
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
}