import React from 'react';
import {
    Image,
    StyleSheet,
} from 'react-native';

import {ImageDictionary} from '../Common/ImageDictionary';

export default class LeafNode extends React.Component{
    render(){
        const imageUrl = ImageDictionary.Leaves["Leaf"+this.props.leafIndex];
        return(
            <Image style={[styles.LeafNode, this.props.style]}
                source={imageUrl}
            ></Image>
        );
    }
    
}

LeafNode.defaultProps = {
    leafIndex: 1,
}

const styles = StyleSheet.create({
    LeafNode:{
        flex:1,
        resizeMode:'contain',
    }
});