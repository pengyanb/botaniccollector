import React from 'react';
import{
    Image,
    StyleSheet,
    Text,
    Dimensions,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class MenuOvalButton extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return(
                <Image style={[styles.ovalButton, this.props.style]} source={ImageDictionary.SpringGUI.OvalMenuBox}>
                    <Text style={styles.ovalButtonText}>{this.props.ovalButtonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Summer){
            return(
                <Image style={[styles.ovalButton, this.props.style]} source={ImageDictionary.SummerGUI.OvalMenuBox}>
                    <Text style={styles.ovalButtonText}>{this.props.ovalButtonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return(
                <Image style={[styles.ovalButton, this.props.style]} source={ImageDictionary.AutumnGUI.OvalMenuBox}>
                    <Text style={styles.ovalButtonText}>{this.props.ovalButtonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return(
                <Image style={[styles.ovalButton, this.props.style]} source={ImageDictionary.WinterGUI.OvalMenuBox}>
                    <Text style={styles.ovalButtonText}>{this.props.ovalButtonText}</Text>
                </Image>
            );
        }
    }
}
MenuOvalButton.defaultProps ={
    seasonIndex: 0,
    buttonText: "Menu Button",
};

const styles = StyleSheet.create({
    ovalButton:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'contain',
    },
    ovalButtonText:{
        backgroundColor:'rgba(0,0,0,0)',
        fontWeight:'bold',
        fontSize:parseInt(Dimensions.get('window').width * 0.04),
    }
});
