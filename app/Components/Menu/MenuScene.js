import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

import {FourSeasonDictionary} from '../Common/GameConstants';

import GameSpinner from '../Common/GameSpinner';
import BackGroundImage from '../Common/BackgroundImage';
import MenuBanner from './MenuBanner';
import MenuArrowButton from './MenuArrowButton';
import MenuOvalButton from './MenuOvalButton';
import MenuGameTitle from './MenuGameTitle';

import {initAppParamFromStorage, persistAppParamToStorage} from '../../Utility/PersistDataHelper';

import {
    createActionSetActivityIndicatorFlag,
    createActionSetSeasonIndex,
} from '../../Redux/Actions/Global.actions';

import {SoundPickUp} from '../../Asserts/Sounds/SoundDictionary';

class MenuScene extends React.Component{
    async componentDidMount(){
        let appParam = await initAppParamFromStorage();
        //console.log('[AppParam]: ' + JSON.stringify(appParam));
        if(appParam === null){
            //console.log("init new App Param");
            appParam = {
                selectedSeasonIndex: 0,
                selectedSpringLevel: 0,
                selectedSummerLevel: 0,
                selectedAutumnLevel: 0,
                selectedWinterLevel: 0,
                springLevelParam:[],
                summerLevelParam:[],
                autumnLevelParam:[],
                winterLevelParam:[],
            };
            await persistAppParamToStorage(appParam);
            this.props.reduxDispatch.setActivityIndicatorFlag(false);
        }
        else{
            this.props.reduxDispatch.setSeasonIndex(appParam.selectedSeasonIndex);
            this.props.reduxDispatch.setActivityIndicatorFlag(false);
        }
    }
    render(){
        return(
                <View style={styles.container}>
                    <GameSpinner isActive={this.props.reduxState.activityIndicatorFlag}/>
                    {this.props.reduxState.activityIndicatorFlag ? null :
                        (<BackGroundImage seasonIndex={this.props.reduxState.seasonIndex}/>)
                    }

                    {this.props.reduxState.activityIndicatorFlag ? null : (
                        <View style={[styles.container, {width:Dimensions.get('window').width, backgroundColor:'rgba(0,0,0,0)'}]}>
                            <View style={{flex:0.5}}/>
                            <MenuGameTitle style={{width: Dimensions.get('window').width*0.8, height:Dimensions.get('window').height*0.4, resizeMode:'contain'}} seasonIndex={this.props.reduxState.seasonIndex}/>
                            <View style={{flex:0.5}}/>
                            <View style={{flex:1, flexDirection:'row'}}>
                                <View style={{flex:1}}/>
                                <View style={{flex:1, flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                                    <TouchableOpacity style={{flex:1, flexDirection:'row',alignItems:'center', justifyContent:'center'}} onPress={()=>{
                                        SoundPickUp.play();
                                        this.props.reduxDispatch.setActivityIndicatorFlag(true);
                                        Actions.CollectionPalletScene();
                                    }}>
                                        <MenuOvalButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} ovalButtonText={"Start"}></MenuOvalButton>
                                    </TouchableOpacity>

                                </View>
                                <View style={{flex:1}}/>
                            </View>
                            <View style={{flex:1}}/>
                            <View style={{flex:1, flexDirection:'row'}}>
                                <View style={{flex:1}}/>
                                <View style={{flex:1, flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                                    <TouchableOpacity style={{flex:1, flexDirection:'row',alignItems:'center', justifyContent:'center'}} onPress={async ()=>{
                                        SoundPickUp.play();
                                        let newSeasonIndex = this.props.reduxState.seasonIndex==3?0:this.props.reduxState.seasonIndex+1;
                                        this.props.reduxDispatch.setSeasonIndex(newSeasonIndex);
                                        await persistAppParamToStorage({selectedSeasonIndex: newSeasonIndex});
                                    }}>
                                        <MenuOvalButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} ovalButtonText={"Season"}></MenuOvalButton>
                                    </TouchableOpacity>

                                </View>
                                <View style={{flex:1}}/>
                            </View>
                            <View style={{flex:1}}/>
                        </View>
                    )}
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF',
    }
});

const mapStateToProps = (state)=>{
    return {reduxState:{
        activityIndicatorFlag: state.reducerSetActivityIndicatorFlag,
        seasonIndex: state.reducerSetSeasonIndex,
    }};
};
const mapDispatchToProps = (dispatch)=>{
    return {reduxDispatch:{
        setActivityIndicatorFlag: (flag)=>{dispatch(createActionSetActivityIndicatorFlag(flag))},
        setSeasonIndex: (index)=>{dispatch(createActionSetSeasonIndex(index))},
    }};
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuScene)
