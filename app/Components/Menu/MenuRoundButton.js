import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    Dimensions,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class MenuRoundButton extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return(
                <Image style={[styles.roundButton, this.props.style]} source={ImageDictionary.SpringGUI.RoundButton}>
                    <Text style={styles.roundButtonText}>{this.props.buttonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Summer){
            return(
                <Image style={[styles.roundButton, this.props.style]} source={ImageDictionary.SummerGUI.RoundButton}>
                    <Text style={styles.roundButtonText}>{this.props.buttonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return(
                <Image style={[styles.roundButton, this.props.style]} source={ImageDictionary.AutumnGUI.RoundButton}>
                    <Text style={styles.roundButtonText}>{this.props.buttonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return(
                <Image style={[styles.roundButton, this.props.style]} source={ImageDictionary.WinterGUI.RoundButton}>
                    <Text style={styles.roundButtonText}>{this.props.buttonText}</Text>
                </Image>
            );
        }
    }
}

MenuRoundButton.defaultProps = {
    seasonIndex: 0,
    buttonText:"Button",
}
const styles = StyleSheet.create({
    roundButton:{
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        resizeMode:'contain',
    },
    roundButtonText:{
        backgroundColor:'rgba(0,0,0,0)',
        fontSize: parseInt(Dimensions.get('window').width * 0.04),
    }
});