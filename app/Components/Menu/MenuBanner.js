import React from 'react';
import {
    Image,
    Text,
    StyleSheet,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class MenuBanner extends React.Component{
    render(){
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return(
                <Image style={[styles.banner, this.props.style]} source={ImageDictionary.SpringGUI.SmallMenuBox}>
                    <Text style={styles.bannerPrimaryText}>{this.props.bannerPrimaryText}</Text>
                    <Text style={styles.bannerSecondaryText}>{FourSeasonDictionary.seasonName(this.props.seasonIndex)}</Text>
                </Image>
            );
        }
        else if (this.props.seasonIndex === FourSeasonDictionary.Summer){
            return(
                <Image style={[styles.banner, this.props.style]} source={ImageDictionary.SummerGUI.SmallMenuBox}>
                    <Text style={styles.bannerPrimaryText}>{this.props.bannerPrimaryText}</Text>
                    <Text style={styles.bannerSecondaryText}>{FourSeasonDictionary.seasonName(this.props.seasonIndex)}</Text>
                </Image>
            );
        }
        else if (this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return(
                <Image style={[styles.banner, this.props.style]} source={ImageDictionary.AutumnGUI.SmallMenuBox}>
                    <Text style={styles.bannerPrimaryText}>{this.props.bannerPrimaryText}</Text>
                    <Text style={styles.bannerSecondaryText}>{FourSeasonDictionary.seasonName(this.props.seasonIndex)}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return(
                <Image style={[styles.banner, this.props.style]} source={ImageDictionary.WinterGUI.SmallMenuBox}>
                    <Text style={styles.bannerPrimaryText}>{this.props.bannerPrimaryText}</Text>
                    <Text style={styles.bannerSecondaryText}>{FourSeasonDictionary.seasonName(this.props.seasonIndex)}</Text>
                </Image>
            );
        }
    }
}

MenuBanner.defaultProps = {
    seasonIndex: 0,
    bannerPrimaryText: 'Menu Banner',
};

const styles = StyleSheet.create({
    banner:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'contain',
    },
    bannerPrimaryText:{
        backgroundColor:'rgba(0,0,0,0)',
        fontWeight:'bold',
        fontSize:20,
    },
    bannerSecondaryText:{
        backgroundColor:'rgba(0,0,0,0)',
        fontSize:12,
        position:'absolute',
        right:'10%',
        bottom:'30%',
        transform:[{rotate:'-45deg'}],
    }

});
