import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    Dimensions,
} from 'react-native';

import {FourSeasonDictionary} from '../Common/GameConstants';
import {ImageDictionary} from '../Common/ImageDictionary';

export default class MenuArrowButton extends React.Component{
    render(){
        const arrowButtonValue = this.props.isLeft ? (this.props.seasonIndex == 0 ? 3 : this.props.seasonIndex - 1)
            : (this.props.seasonIndex == 3 ? 0 : this.props.seasonIndex + 1);
        const arrowButtonText = this.props.buttonText || FourSeasonDictionary.seasonName(arrowButtonValue);
        if(this.props.seasonIndex === FourSeasonDictionary.Spring){
            return(
                <Image style={[styles.arrow, this.props.style, (this.props.isLeft?{transform:[{rotate:'180deg'}]}:{})]} source={ImageDictionary.SpringGUI.ArrowButton}>
                    <Text style={[styles.arrowText, (this.props.isLeft?{transform:[{rotate:'-180deg'}]}:{})]}>{arrowButtonText}</Text>
                </Image>
            );
        }
        else if (this.props.seasonIndex === FourSeasonDictionary.Summer){
            return(
                <Image style={[styles.arrow, this.props.style, (this.props.isLeft?{transform:[{rotate:'180deg'}]}:{})]} source={ImageDictionary.SummerGUI.ArrowButton}>
                    <Text style={[styles.arrowText, (this.props.isLeft?{transform:[{rotate:'-180deg'}]}:{})]}>{arrowButtonText}</Text>
                </Image>
            );
        }
        else if (this.props.seasonIndex === FourSeasonDictionary.Autumn){
            return(
                <Image style={[styles.arrow, this.props.style, (this.props.isLeft?{transform:[{rotate:'180deg'}]}:{})]} source={ImageDictionary.AutumnGUI.ArrowButton}>
                    <Text style={[styles.arrowText, (this.props.isLeft?{transform:[{rotate:'-180deg'}]}:{})]}>{arrowButtonText}</Text>
                </Image>
            );
        }
        else if(this.props.seasonIndex === FourSeasonDictionary.Winter){
            return(
                <Image style={[styles.arrow, this.props.style, (this.props.isLeft?{transform:[{rotate:'180deg'}]}:{})]} source={ImageDictionary.WinterGUI.ArrowButton}>
                    <Text style={[styles.arrowText, (this.props.isLeft?{transform:[{rotate:'-180deg'}]}:{})]}>{arrowButtonText}</Text>
                </Image>
            );
        }
    }
}

MenuArrowButton.defaultProps = {
    seasonIndex: 0,
    isLeft:false,
};
const styles = StyleSheet.create({
    arrow:{
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        resizeMode:'contain',
    },
    arrowText:{
        backgroundColor:'rgba(0,0,0,0)',
        fontSize: parseInt(Dimensions.get('window').width * 0.04),
    }
});
