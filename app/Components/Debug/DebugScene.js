import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

import Svg, {
    Circle, 
} from 'react-native-svg';

import {FourSeasonDictionary} from '../Common/GameConstants';

import GameSpinner from '../Common/GameSpinner';
import BackGroundImage from '../Common/BackgroundImage';
import MenuRoundButton from '../Menu/MenuRoundButton';
import MenuArrowButton from '../Menu/MenuArrowButton';
import LeafNode from '../Game/LeafNode';

import {
    createActionSetActivityIndicatorFlag,
    createActionSetSeasonIndex,
} from '../../Redux/Actions/Global.actions';

import {
    createActionInitializeCollectionPallet,
    createActionInitStartGrid,
    createActionInitEndGrid,
    createActionSetBlockedGrid,
    createActionFindPath,
} from '../../Redux/Actions/CollectionPallet.actions';

import {SoundPickUp} from '../../Asserts/Sounds/SoundDictionary';

class DebugScene extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            needToInitializeCollectionPallet:true,
            currentOperation:"Set Start",
            leafStartIndex:1,
            leafEndIndex:21,
            lastTouchMovedKey:'',
        };
    }
    componentDidMount(){
        if(this.state.needToInitializeCollectionPallet){
            this.setState({needToInitializeCollectionPallet:false});
            this.setState({leafStartIndex:Math.floor(Math.random()*21+1)});
            this.setState({leafEndIndex:Math.floor(Math.random()*21+1)});
            this.props.reduxDispatch.initializeCollectionPallet();
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.props.reduxState.seasonIndex !== nextProps.reduxState.seasonIndex){
            this.setState({needToInitializeCollectionPallet:true});
        }
    }
    componentDidUpdate(){
        if(this.state.needToInitializeCollectionPallet){
            this.setState({needToInitializeCollectionPallet:false});
            this.setState({leafStartIndex:Math.floor(Math.random()*21+1)});
            this.setState({leafEndIndex:Math.floor(Math.random()*21+1)});
            this.props.reduxDispatch.initializeCollectionPallet();
        }
        //this.refs.gameTouchContainer.onStartShouldSetResponder();
        //this.refs.gameTouchContainer.onMoveShouldSetResponder();
    }

    handleGameTouchMoveEvent(evt, isStart){
        console.log("[handleGameTouchMoveEvent] start");
        // console.log("identifier: "+ evt.nativeEvent.identifier);
        // console.log("locationX: "+ evt.nativeEvent.locationX);
        // console.log("locationY: "+ evt.nativeEvent.locationY);
        // console.log("pageX: "+ evt.nativeEvent.pageX);
        // console.log("pageY: "+ evt.nativeEvent.pageY);
        // console.log("target: "+ evt.nativeEvent.target);
        // console.log("[handleGameTouchMoveEvent] end");

        let tapLocationX = evt.nativeEvent.pageX;
        let tapLocationY = evt.nativeEvent.pageY;

        for(var i=0; i<this.props.reduxState.palletSize.rowCount; i++){
            for(var j=0; j<this.props.reduxState.palletSize.columnCount; j++){
                const rIndex = i;
                const cIndex = j;
                const refKey = "r"+rIndex+"c"+cIndex;
                this.refs[refKey].measure((ox, oy, width, height, px, py)=>{
                    if((tapLocationX > px) && (tapLocationX < (px + width)) && (tapLocationY > py) && (tapLocationY < (py + height) )){
                        //this.refs[refKey].touchMoveHandler();   
                            console.log(refKey);
                            if(refKey != this.state.lastTouchMovedKey){
                                this.setState({lastTouchMovedKey: refKey});
                                if(isStart){
                                if(this.state.currentOperation == "Set Start"){
                                    this.props.reduxDispatch.initStartGrid(rIndex, cIndex);
                                }
                                else if(this.state.currentOperation == "Set End"){
                                    this.props.reduxDispatch.initEndGrid(rIndex, cIndex);
                                }
                                SoundPickUp.play();
                            }     
                            if(this.state.currentOperation == "Add Block"){
                                let grid = this.props.reduxState.gridMatrix[rIndex][cIndex];
                                this.props.reduxDispatch.addBlockedGrid(rIndex, cIndex, !grid.isBlocked);
                            }                
                        }
                    }
                });
            }
        }
    }


    render(){
        const gridWidth = Dimensions.get('window').width * 0.9 / this.props.reduxState.palletSize.columnCount;
        const gridHeigh = Dimensions.get('window').height * 0.6 / this.props.reduxState.palletSize.rowCount;
        const unitSize = gridWidth < gridHeigh ? (gridWidth-2) : (gridHeigh-2);

        return(
            <View style={styles.container}>
                <GameSpinner isActive={this.props.reduxState.activityIndicatorFlag}/>
                {this.props.reduxState.activityIndicatorFlag ? null :
                    (<BackGroundImage seasonIndex={this.props.reduxState.seasonIndex}/>)
                }
                {this.props.reduxState.activityIndicatorFlag ? null : (
                    <View style={{flexDirection:'column', paddingTop:20, width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.30, backgroundColor:'rgba(0,0,0,0)'}}>
                        <View style={{flex:1,width:Dimensions.get('window').width,flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                            <TouchableOpacity key={"menu0"} style={{width:Dimensions.get('window').height*0.05, height:Dimensions.get('window').height*0.05, flexDirection:'column', alignItems:'center', justifyContent:'center',}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    this.setState({currentOperation:"Set Start"});
                                }}>
                                <MenuRoundButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} roundButtonText={"Set Start"}/>
                            </TouchableOpacity>
                            
                            <TouchableOpacity key={"menu1"} style={{width:Dimensions.get('window').height*0.05, height:Dimensions.get('window').height*0.05, flexDirection:'column', alignItems:'center', justifyContent:'center',}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    this.setState({currentOperation:"Set End"});
                                }}>
                                <MenuRoundButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} roundButtonText={"Set End"}/>
                            </TouchableOpacity>
                            
                            <TouchableOpacity key={"menu2"} style={{width:Dimensions.get('window').height*0.05, height:Dimensions.get('window').height*0.05,flexDirection:'column', alignItems:'center', justifyContent:'center',}}
                                onPress={()=>{
                                        SoundPickUp.play();
                                        this.setState({currentOperation:"Add Block"});
                                    }}>
                                <MenuRoundButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} roundButtonText={"Add Block"}/>
                            </TouchableOpacity>
                            
                            <TouchableOpacity key={"menu3"} style={{width:Dimensions.get('window').height*0.05, height:Dimensions.get('window').height*0.05,flexDirection:'column', alignItems:'center', justifyContent:'center',}}
                                onPress={()=>{
                                    SoundPickUp.play();
                                    this.setState({currentOperation:"Path Finding"});
                                    this.props.reduxDispatch.findPath();
                                }}>
                                <MenuRoundButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} roundButtonText={"Find Path"}/>
                            </TouchableOpacity>
                            
                        </View>
                        <View style={{flex:1,width:Dimensions.get('window').width,flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                            <TouchableOpacity style={{width:Dimensions.get('window').height*0.1, height:Dimensions.get('window').height*0.1, flexDirection:'column', alignItems:'center', justifyContent:'center',}} onPress={()=>{
                                SoundPickUp.play();
                                this.props.reduxDispatch.setSeasonIndex(this.props.reduxState.seasonIndex==0?3:this.props.reduxState.seasonIndex-1);
                            }}>
                                <MenuArrowButton style={{flex:1}} seasonIndex={this.props.reduxState.seasonIndex} isLeft={true}/>
                            </TouchableOpacity>
                            <Text>{this.state.currentOperation}</Text>
                            <TouchableOpacity style={{width:Dimensions.get('window').height*0.1, height:Dimensions.get('window').height*0.1, flexDirection:'column', alignItems:'center', justifyContent:'center',}} onPress={()=>{
                                SoundPickUp.play();
                                this.props.reduxDispatch.setSeasonIndex(this.props.reduxState.seasonIndex==3?0:this.props.reduxState.seasonIndex+1);
                            }}>
                                <MenuArrowButton style={{flex:1}}  seasonIndex={this.props.reduxState.seasonIndex} isLeft={false}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
                {this.props.reduxState.activityIndicatorFlag ? null :(
                    <View style={[styles.container, {justifyContent:'flex-end', width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.65, backgroundColor:'rgba(0,0,0,0)'}]}
                            onStartShouldSetResponder={(evt)=>{return true;}}
                            onMoveShouldSetResponder={(evt)=>{return true;}}
                            onStartShouldSetResponderCapture={(evt)=>{return true;}}
                            onMoveShouldSetResponderCapture={(evt)=>{return true;}}
                            ref={"gameTouchContainer"}
                            onResponderStart={(evt)=>{
                                this.handleGameTouchMoveEvent(evt, true);
                            }}
                            onResponderMove={(evt)=>{
                                this.handleGameTouchMoveEvent(evt, false);
                            }}
                        >
                        {this.props.reduxState.gridMatrix.map((row, rIndex)=>{
                            return (<View key={"r"+rIndex} style={{flexDirection:'row', justifyContent:'center', alignItems:'center',width:Dimensions.get('window').width*0.9}}>
                                {row.map((grid, cIndex)=>{
                                    const refKey = "r"+rIndex+"c"+cIndex;
                                    return (<View key={"c"+cIndex} style={{flex:1, flexDirection:'column', padding:1, justifyContent:'center', alignItems:'center'}}>
                                                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', width:unitSize, height:unitSize, backgroundColor:'rgba(120,144,156, 0.9)'}}
                                                    ref={refKey}
                                                    >
                                                    {grid.isStartGrid?(
                                                        <Svg height={unitSize} width={unitSize} style={{position:'absolute', top:0, left:0}}>
                                                            <Circle cx={unitSize*0.5} 
                                                                    cy={unitSize*0.5} 
                                                                    r={unitSize*0.4}
                                                                    fill={'red'}/>
                                                        </Svg>):null}
                                                    {grid.isStartGrid?(<LeafNode style={{width:unitSize*0.6, height:unitSize*0.6}} leafIndex={this.state.leafStartIndex}></LeafNode>):null}
                                                    {grid.isEndGrid?(<LeafNode leafIndex={this.state.leafEndIndex}></LeafNode>):null}
                                                    {grid.isBlocked?(<View style={{flex:1,height:unitSize, backgroundColor:'red'}}></View>):null}
                                                    {grid.isPath?(<View style={{flex:1,height:unitSize, backgroundColor:'white'}}></View>):null}
                                                </View>
                                            </View>)
                                })}
                                    </View>)
                        })}
                    </View>
                )}
                {this.props.reduxState.activityIndicatorFlag ? null : (
                    <View style={{width:Dimensions.get('window').width, height:Dimensions.get('window').height*0.05, backgroundColor:'rgba(0,0,0,0)'}}/>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF',
    }
});

const mapStateToProps = (state)=>{
    console.log('[PalletSize]: ' + JSON.stringify(state.reducerSetPalletSize));
    return {reduxState:{
        activityIndicatorFlag: state.reducerSetActivityIndicatorFlag,
        seasonIndex: state.reducerSetSeasonIndex,
        palletSize: state.reducerSetPalletSize,
        gridMatrix: state.reducerSetGridMatrix,
    }};
};

const mapDispatchToProps = (dispatch)=>{
    return {reduxDispatch:{
        setActivityIndicatorFlag: (flag)=>{dispatch(createActionSetActivityIndicatorFlag(flag))},
        setSeasonIndex: (index)=>{dispatch(createActionSetSeasonIndex(index))},
        initializeCollectionPallet:()=>{dispatch(createActionInitializeCollectionPallet())},
        initStartGrid: (rowIndex, columnIndex)=>{dispatch(createActionInitStartGrid(rowIndex, columnIndex))},
        initEndGrid:(rowIndex, columnIndex)=>{dispatch(createActionInitEndGrid(rowIndex, columnIndex))},
        addBlockedGrid:(rowIndex, columnIndex, isSet)=>{dispatch(createActionSetBlockedGrid(rowIndex, columnIndex, isSet))},
        findPath:()=>{dispatch(createActionFindPath())},
    }};
};
export default connect(mapStateToProps, mapDispatchToProps, null, {withRef: true})(DebugScene);