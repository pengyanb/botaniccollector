export function pathFindingRoutineForNodePair(startGrid, endGrid, matrixModel, rowCount, columnCount){
    let openNodeArray = [];
    let closedNodeArray = [];
    let matrix = [];
    for(let i=0; i<matrixModel.length; i++){
        let row = [];
        for(let j=0; j<matrixModel[i].length; j++){
            row.push(Object.assign({}, matrixModel[i][j]));
        }
        matrix.push(row);
    }

    let startGridNode = matrix[startGrid.rowIndex][startGrid.columnIndex];
    let endGridNode = matrix[endGrid.rowIndex][endGrid.columnIndex];
    for(let i=0; i<rowCount; i++){
        for(let j=0; j<columnCount; j++){
            matrix[i][j].parentRowIndex = null;
            matrix[i][j].parentColumnIndex = null;
            matrix[i][j].gCost = null;
            matrix[i][j].hCost = null;
            matrix[i][j].fCost = null;
        }
    }
    openNodeArray.push(startGridNode);
    let found = false;
    let completed = false;
    while((!found) && (!completed)){
        let currentGridNode = null;
        let lowestFCost = Number.POSITIVE_INFINITY;
        for(let i=0; i<openNodeArray.length; i++){
            let thisNode = openNodeArray[i];
            if((thisNode.parentColumnIndex === null) && (thisNode.parentColumnIndex === null)){
                thisNode.gCost = Math.abs(thisNode.rowIndex - startGridNode.rowIndex) + Math.abs(thisNode.columnIndex - startGridNode.columnIndex);
                thisNode.hCost = Math.abs(thisNode.rowIndex - endGridNode.rowIndex) + Math.abs(thisNode.columnIndex - endGridNode.columnIndex);
                thisNode.fCost = thisNode.gCost + thisNode.hCost;
            }
            else{
                thisNode.gCost = matrix[thisNode.parentRowIndex][thisNode.parentColumnIndex].gCost + 1;
                thisNode.hCost = Math.abs(thisNode.rowIndex - endGridNode.rowIndex) + Math.abs(thisNode.columnIndex - endGridNode.columnIndex);
                thisNode.fCost = thisNode.gCost + thisNode.hCost;
            }
            if(thisNode.fCost < lowestFCost){
                lowestFCost = thisNode.fCost;
                currentGridNode = thisNode;
            }
        }
        if(currentGridNode !== null){
            openNodeArray = removePathGrid(openNodeArray, currentGridNode);
            closedNodeArray.push(currentGridNode);
            //if(Object.is(currentGridNode, endGridNode)){
            if((currentGridNode.rowIndex === endGridNode.rowIndex) && (currentGridNode.columnIndex === endGridNode.columnIndex)){
                found = true;
            }
            else{
                var neighbourGridNodeArray = [];
                if((currentGridNode.rowIndex-1)>=0){    //top neighbour
                    var neighbourGridNode = matrix[currentGridNode.rowIndex-1][currentGridNode.columnIndex];
                    neighbourGridNodeArray.push(neighbourGridNode);
                }
                if((currentGridNode.rowIndex+1)<rowCount){  //bottom neighbour
                    neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex+1][currentGridNode.columnIndex]);
                }
                if((currentGridNode.columnIndex-1)>=0){     //left neighbour
                    neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex][currentGridNode.columnIndex-1]);
                }
                if((currentGridNode.columnIndex+1)<columnCount){    //right neighbour
                    neighbourGridNodeArray.push(matrix[currentGridNode.rowIndex][currentGridNode.columnIndex+1]);
                }

                for(var i = 0; i < neighbourGridNodeArray.length; i++){ 
                    let neighbourGridNode = neighbourGridNodeArray[i];
                    //if(neighbourGridNode.isBlocked && !Object.is(neighbourGridNode, endGridNode)){
                    if(neighbourGridNode.isBlocked && ((neighbourGridNode.rowIndex !== endGridNode.rowIndex) || (neighbourGridNode.columnIndex !== endGridNode.columnIndex))){
                        continue;   //skip to the next neighbour
                    }
                    if(containsPathGrid(closedNodeArray, neighbourGridNode)){
                        continue;
                    }

                    let newGCost = currentGridNode.gCost + 1;
                    let newHCost = Math.abs(neighbourGridNode.rowIndex - endGridNode.rowIndex) + Math.abs(neighbourGridNode.columnIndex - endGridNode.columnIndex);
                    let newFCost = newGCost + newFCost;
                    if(neighbourGridNode.fCost == null){
                        neighbourGridNode.parentRowIndex = currentGridNode.rowIndex;
                        neighbourGridNode.parentColumnIndex = currentGridNode.columnIndex;
                        neighbourGridNode.gCost = newGCost;
                        neighbourGridNode.hCost = newHCost;
                        neighbourGridNode.fCost = newFCost;
                        //console.log("assign parentRowIndex 1" + JSON.stringify(neighbourGridNode));
                    }
                    else if(neighbourGridNode.fCost >= newFCost){
                        neighbourGridNode.parentRowIndex = currentGridNode.rowIndex;
                        neighbourGridNode.parentColumnIndex = currentGridNode.columnIndex;
                        neighbourGridNode.gCost = newGCost;
                        neighbourGridNode.hCost = newHCost;
                        neighbourGridNode.fCost = newFCost;
                        //console.log("assign parentRowIndex 2" + JSON.stringify(neighbourGridNode));
                    }

                    if(!containsPathGrid(openNodeArray, neighbourGridNode)){
                        // console.log("[openNodeArray Push]: " + JSON.stringify(neighbourGridNode));
                        // console.log("Bool: " + neighbourGridNode.isBlock );
                        openNodeArray.push(neighbourGridNode);
                    }
                }
            }
        }
        else{
            completed = true;
        }
        if(closedNodeArray.length >= (rowCount * columnCount)){
            completed = true;
        }
    }
    if(found){
        let done = false;
        var currentNode = endGridNode;
        var pathNodeArray = [Object.assign({}, currentNode)];
        let leafBackgroundColor = currentNode.leafBackgroundColor;
        while(!done){
            currentNode.isBlocked = true;
            //if(Object.is(currentNode, startGridNode)){
            if((currentNode.rowIndex === startGridNode.rowIndex) && (currentNode.columnIndex === startGridNode.columnIndex)){
                done = true;
            }
            else{
                currentNode = matrix[currentNode.parentRowIndex][currentNode.parentColumnIndex];
                currentNode.leafBackgroundColor = leafBackgroundColor;
                pathNodeArray.push(Object.assign({}, currentNode));
            }
        }
        return pathNodeArray;
    }
    else{
        return [];
    }
}

function removePathGrid(nodeArray, gridNode) {
    for (var i = 0; i < nodeArray.length; i++) {
        if ((nodeArray[i].rowIndex === gridNode.rowIndex) && (nodeArray[i].columnIndex === gridNode.columnIndex)) {
            nodeArray.splice(i, 1);
            i--;
        }
    }
    return nodeArray;
};

function containsPathGrid(nodeArray, gridNode){
    for(var i = 0; i < nodeArray.length; i++){
        if((nodeArray[i].rowIndex === gridNode.rowIndex) && (nodeArray[i].columnIndex === gridNode.columnIndex)) {
            return true;
        }
    }
    return false;
};