import {
    AsyncStorage,
} from 'react-native';

export async function persistAppParamToStorage(appParam){
    try{
        await AsyncStorage.mergeItem('AppParam', JSON.stringify(appParam));
        return true;
    }
    catch(error){
        console.log("[Persist AppParam To Storage Error]: ", error);
        return false;
    }
}

export async function initAppParamFromStorage(){
    try{
        const appParamString = await AsyncStorage.getItem('AppParam');
        if(appParamString === null){
            return null;
        }
        else{
            return JSON.parse(appParamString);
        }
        
    }
    catch(error){
        console.log("[Init AppParam From Storage]: ", error);
        return null;
    }
}