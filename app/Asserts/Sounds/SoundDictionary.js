import Sound from 'react-native-sound';

export const SoundGameOver = new Sound('gameover.mp3', Sound.MAIN_BUNDLE,  (err)=>{
    if(err){
        console.log(err);
    }
});
export const SoundLevelClear = new Sound('levelclear.mp3', Sound.MAIN_BUNDLE,  (err)=>{
    if(err){
        console.log(err);
    }
});
// export const SoundMenu = new Sound('Menu.mp3', Sound.MAIN_BUNDLE, (err)=>{
//     if(err){
//         console.log(err);
//     }
// });
export const SoundPickUp = new Sound('pickup.mp3', Sound.MAIN_BUNDLE,  (err)=>{
    if(err){
        console.log(err);
    }
});