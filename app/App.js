import React from 'react';
import {
    StyleSheet,
    AsyncStorage,
    Text,
} from 'react-native';
import {Scene, Router, Actions, ActionConst} from 'react-native-router-flux';
import {connect, Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import Sound from 'react-native-sound';

import AppReducer from './Redux/Reducers/AppReducer';

import MenuScene from './Components/Menu/MenuScene';
import CollectionPalletScene from './Components/Game/CollectionPalletScene';
import DebugScene from './Components/Debug/DebugScene';

const RouterWithRedux = connect()(Router);
const logger = createLogger();
const store = createStore(AppReducer, applyMiddleware(thunk, promise));

const scenes = Actions.create(
    <Scene key="root">
        <Scene key="MenuScene" component={MenuScene} title="Menu" hideNavBar={true} statusBarStyle="light-content" initial={true} type={ActionConst.RESET}/>
        <Scene key="CollectionPalletScene" panHandlers={null} component={CollectionPalletScene} title="" hideNavBar={true} statusBarStyle="light-content" type={ActionConst.REST}/>
    </Scene>
);
//<Scene key="DebugScene" component={DebugScene} title="" hideNavBar={true} statusBarStyle="light-content" type={ActionConst.RESET}/>
export default class App extends React.Component{
    backgroundMusicHandler(err){
        if(err){
            console.log('failed to load the sound', err);
            return;
        }
        this.SoundBackgroundMuisc.play(()=>{
            console.log("[SoundBackgroundMusic finished]");
            this.backgroundMusicHandler();
        });
    }
    constructor(props){
        super(props);
        this.SoundBackgroundMuisc = null;
    }
    componentDidMount(){
        this.SoundBackgroundMuisc = new Sound('backgroundmusic.mp3', Sound.MAIN_BUNDLE, this.backgroundMusicHandler.bind(this));
    }
    componentWillUnmount(){
        if(this.SoundBackgroundMuisc !== null){
            this.SoundBackgroundMuisc.stop();
            this.SoundBackgroundMuisc.release();
        }
    }
    render(){
        return(
            <Provider store={store}>
                <RouterWithRedux scenes={scenes}></RouterWithRedux>
            </Provider>
        );
    }
}